package analytics

import(
	"fmt"
	"parserNews/tools"
)

type ComparisonZakon struct{
	Title string
	KeyState string
}

type NewsTime struct{
	Key string
	News *map[string]string
}

type ComparisonZakonNurTengry struct{
	NewsTimes map[string]string
	ZakonTime NewsTime
	TengryTime NewsTime
	NurTime NewsTime
}

var comparisonsZakon []ComparisonZakon
var comparisonsZakonNurTengry []ComparisonZakonNurTengry


// TODO refactoring
func Comparison(mainSlice tools.StateNews, slices []tools.StateNews) {
	for mainKey, mainValue := range mainSlice.News.Value {
		for _, stateNews := range slices {
			for key, value := range stateNews.News.Value {
		    	title := clearStringThree(mainValue["title"])
		    	comparisonTitle := clearStringThree(value["title"])

		        similarity := levenshteinDistance(title, comparisonTitle)
		        jsimilarity := jaccardIndex(title, comparisonTitle)
		        cossimilarity := cosineSimilarity(title, comparisonTitle)

		        if similarity <= 70 && jsimilarity >= 0.2 && cossimilarity > 0.43 {
		        	mainSlice.News.Value[mainKey]["similarity"] = "true"
		            stateNews.News.Value[key]["similarity"] = "true"
		            stateNews.News.Value[key]["zakonKey"] = fmt.Sprintf("%d", mainKey + 1)

		            if checkFullComparisonZakon(mainValue["title"], stateNews.Name) {
		            	mainSlice.News.Value[mainKey]["doubtful"] = "true"
		            } else {
		            	comparisonsZakon = append(comparisonsZakon, ComparisonZakon{
			            	Title: mainValue["title"],
			            	KeyState: stateNews.Name,
			            })
		            }


		            if keyComparison := checkComparisonZakonNurTengry(mainValue["title"]); keyComparison != -1 {
		            	if mainSlice.News.Value[mainKey]["doubtful"] == "true" {
		            		comparisonsZakonNurTengry[keyComparison].NewsTimes["doubtful"] = "true"
		            	}
		            	comparisonsZakonNurTengry[keyComparison].NewsTimes[stateNews.Name] = value["time"]

		            	switch stateNews.Name {
		            		case "Nur":
		            			comparisonsZakonNurTengry[keyComparison].NurTime = NewsTime{
		            				News: &stateNews.News.Value[key],
		            				Key: stateNews.Name,
		            			}
		            		case "Tengry":
		            			comparisonsZakonNurTengry[keyComparison].TengryTime = NewsTime{
		            				News: &stateNews.News.Value[key],
		            				Key: stateNews.Name,
		            			}
		            	}
		            } else {
		            	comparisonZakonNurTengry := map[string]string{
		            		"title": mainValue["title"],
		            		"Zakon": mainValue["time"],
		            		stateNews.Name: value["time"],
		            	}

		            	_, ok := mainSlice.News.Value[mainKey]["doubtful"]
		            	if ok {
		            		comparisonZakonNurTengry["doubtful"] = "true"
		            	}

		            	comparisonZakonNurTengryStruct := ComparisonZakonNurTengry{
		            		NewsTimes: comparisonZakonNurTengry,
		            		ZakonTime: NewsTime{
		            			News: &mainSlice.News.Value[mainKey],
		            			Key: "Zakon",
		            		},
		            	}
		            	switch stateNews.Name {
		            		case "Nur":
		            			comparisonZakonNurTengryStruct.NurTime = NewsTime{
		            				News: &stateNews.News.Value[key],
		            				Key: stateNews.Name,
		            			}
		            		case "Tengry":
		            			comparisonZakonNurTengryStruct.TengryTime = NewsTime{
		            				News: &stateNews.News.Value[key],
		            				Key: stateNews.Name,
		            			}
		            	}

		            	comparisonsZakonNurTengry = append(comparisonsZakonNurTengry, comparisonZakonNurTengryStruct)
		            }
		        }
			}
		}
	}
}

func SearchWhyFirstFromTime() {
	for _, value := range comparisonsZakonNurTengry {
		min := value.NewsTimes["Zakon"]
		minKey := ""
		allEqual := true

		for comparisonKey, comparisonValue := range value.NewsTimes {
			if comparisonKey != "Zakon" && comparisonKey != "Tengry" && comparisonKey != "Nur" {
				continue
			}

			if comparisonValue != min {
				allEqual = false
			}

			if comparisonValue < min {
				min = comparisonValue
				minKey = comparisonKey
			} else if min == comparisonValue {
				minKey = comparisonKey
			}
		}

		if !allEqual {
			switch minKey {
				case "Tengry":
					if value.TengryTime.News != nil {
						(*value.TengryTime.News)["color"] = "00ff05"
					}
					(*value.ZakonTime.News)["color"] = "ff0000"
					if value.NurTime.News != nil {
						(*value.NurTime.News)["color"] = "ff0000"
					}
				case "Nur":
					if value.TengryTime.News != nil {
						(*value.TengryTime.News)["color"] = "ff0000"
					}
					(*value.ZakonTime.News)["color"] = "ff0000"
					if value.NurTime.News != nil  {
						(*value.NurTime.News)["color"] = "00ff05"
					}
				case "Zakon":
					if value.TengryTime.News != nil {
						(*value.TengryTime.News)["color"] = "ff0000"
					}
					(*value.ZakonTime.News)["color"] = "00ff05"
					if value.NurTime.News != nil  {
						(*value.NurTime.News)["color"] = "ff0000"
					}
			}
		}
	}
}

func checkComparisonZakon(title, keyState string) bool {
	for _, value := range comparisonsZakon {
		if value.Title == title {
			return true
		}
	}

	return false
}

func checkFullComparisonZakon(title, keyState string) bool {
	for _, value := range comparisonsZakon {
		if value.Title == title && value.KeyState == keyState {
			return true
		}
	}

	return false
}

func checkComparisonZakonNurTengry(title string) int {
	for key, value := range comparisonsZakonNurTengry {
		if value.NewsTimes["title"] == title {
			return key
		}
	}

	return -1
}