package analytics

import (
	"math"
	"regexp"
	"strings"
	"github.com/kljensen/snowball"
)

func jaccardIndex(s1, s2 string) float64 {
	set1 := make(map[string]struct{})
	set2 := make(map[string]struct{})

	words1 := strings.Fields(s1)
	words2 := strings.Fields(s2)

	for _, word := range words1 {
		set1[word] = struct{}{}
	}

	for _, word := range words2 {
		set2[word] = struct{}{}
	}

	intersection := 0
	for word := range set1 {
		if _, ok := set2[word]; ok {
			intersection++
		}
	}

	union := len(set1) + len(set2) - intersection
	return float64(intersection) / float64(union)
}

func levenshteinDistance(s1, s2 string) int {
	m := len(s1)
	n := len(s2)

	d := make([][]int, m+1)
	for i := range d {
		d[i] = make([]int, n+1)
	}

	for i := 0; i <= m; i++ {
		d[i][0] = i
	}
	for j := 0; j <= n; j++ {
		d[0][j] = j
	}

	for j := 1; j <= n; j++ {
		for i := 1; i <= m; i++ {
			cost := 0
			if s1[i-1] != s2[j-1] {
				cost = 1
			}
			d[i][j] = min(d[i-1][j]+1, d[i][j-1]+1, d[i-1][j-1]+cost)
		}
	}

	return d[m][n]
}

func min(a, b, c int) int {
	if a <= b && a <= c {
		return a
	} else if b <= a && b <= c {
		return b
	}
	return c
}

func findRoots(words []string) []string {
	var roots []string
    for _, word := range words {
        root, _ := snowball.Stem(word, "russian", true)
        roots = append(roots, root)
    }
    return roots
}


func clearStringThree(str string) string {
	str = strings.ToLower(str)
	var re = regexp.MustCompile(`(?m)([а-я]|\d){3,}`)
	strSlice := re.FindAllString(str, -1)

	return strings.Join(findRoots(strSlice), " ")
}

func cosineSimilarity(s1, s2 string) float64 {
	vector1 := createVector(s1)
	vector2 := createVector(s2)

	dotProduct := dotProduct(vector1, vector2)
	magnitude1 := magnitude(vector1)
	magnitude2 := magnitude(vector2)

	return dotProduct / (magnitude1 * magnitude2)
}

func createVector(s string) map[string]int {
	vector := make(map[string]int)
	words := strings.Fields(s)

	for _, word := range words {
		vector[word]++
	}

	return vector
}

func dotProduct(v1, v2 map[string]int) float64 {
	sum := 0.0

	for word, count := range v1 {
		sum += float64(count * v2[word])
	}

	return sum
}

func magnitude(v map[string]int) float64 {
	sum := 0.0

	for _, count := range v {
		sum += float64(count * count)
	}

	return math.Sqrt(sum)
}