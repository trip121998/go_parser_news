package server

import (
	"fmt"
	"net/http"
	"html/template"
	"parserNews/news"
	"parserNews/tools"
)

func downloadPage(w http.ResponseWriter, r *http.Request) {
	dateBegin := r.URL.Query().Get("date_start");
	dateEnd := r.URL.Query().Get("date_end");

    f, err := news.GetExcelAllNews(dateBegin, dateEnd)
    if err != nil {
    	w.Write([]byte("Error"));
    	return;
    }

	w.Header().Set("Content-Disposition", "attachment; filename=test.xlsx")
	w.Header().Set("Content-Type", r.Header.Get("Content-Type"))
	w.Header().Set("Content-Length", r.Header.Get("Content-Length"))
	w.Header().Set("Access-Control-Allow-Origin", "*");
    w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

	f.Write(w)
}

func homePage(w http.ResponseWriter, r *http.Request) {
	t, err := template.New("webpage").Parse(tools.Html)
	if err != nil {
		w.Write([]byte("Error"));
	}

	t.Execute(w, nil)
	if err != nil {
		w.Write([]byte("Error"));
	}
}

func Start() {
	http.HandleFunc("/", homePage);
	http.HandleFunc("/download", downloadPage)
	err := http.ListenAndServe(":8000", nil)

	if err != nil {
		fmt.Println(err)
	}
}