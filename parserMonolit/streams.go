package main

import (
	"fmt"
	"time"
)

func main() {
	a := make(chan []int);
	b := make(chan []int);
	c := make(chan []int);

	go zakon(a);
	go tengry(b);
	go nur(c);

	for i := 0; i < 3; i++ {	
		select {
		case ma := <-a:
			fmt.Println("zakon", ma);
		case mb := <-b:
			fmt.Println("tengry", mb);
		case mc := <-c:
			fmt.Println("nur", mc);
		}
	}
}

func zakon(a chan []int) {
	var buffer []int
	for i := 0; i < 10; i++ {
		fmt.Println("go func zakon.", i+1);
		buffer = append(buffer, i+1)
		time.Sleep(1);
	}
	a <- buffer;
}

func tengry(b chan []int) {
	var buffer []int
	for i := 0; i < 10; i++ {
		fmt.Println("go func tengry.", i+1);
		buffer = append(buffer, i+1);
		time.Sleep(1);
	}
	b <- buffer;
}

func nur(c chan []int) {
	var buffer []int
	for i := 0; i < 10; i++ {
		fmt.Println("go func nur.", i+1);
		buffer = append(buffer, i+1);
		time.Sleep(1);
	}
	c <- buffer;
}