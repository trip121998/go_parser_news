package main

import (
	"fmt"
	"net/http"
	"html/template"
	"github.com/xuri/excelize/v2"
)

var Headers = map[string][]string{
	"№": []string{"A", "J", "R"},
	"Заголовок": []string{"B", "K", "S"},
	"Символы": []string{"C", "L", "T"},
	"Ссылка": []string{"D", "M", "U"},
	"Просмотры": []string{"N"},
	"Время": []string{"E", "O", "V"},
	"Дата": []string{"F", "P", "W"},
}

func generateExcel() *excelize.File{
	f := excelize.NewFile()
    defer func() {
        if err := f.Close(); err != nil {
            fmt.Println(err)
        }
    }()

    f.SetColWidth("Sheet1", "A", "W", 15)

    for k, v := range Headers {
    	for _, v1 := range v {
    		f.SetCellValue("Sheet1", v1+"1", k);
    	}
    }

    return f
}

func Index(w http.ResponseWriter, r *http.Request) {
	dateBegin := r.URL.Query().Get("date_start")
	dateEnd := r.URL.Query().Get("date_end")

    f := generateExcel()

	w.Header().Set("Content-Disposition", "attachment; filename=Wiki.png")
	w.Header().Set("Content-Type", r.Header.Get("Content-Type"))
	w.Header().Set("Content-Length", r.Header.Get("Content-Length"))
	w.Header().Set("Access-Control-Allow-Origin", "*");
    w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

	f.Write(w)
}

func home(w http.ResponseWriter, r *http.Request) {
	t, err := template.New("webpage").Parse(html)
	fmt.Println(err)
	t.Execute(w, nil)
	if err != nil {
		fmt.Println(err)
	}
}

func main() {
	http.HandleFunc("/", home);
	http.HandleFunc("/download", Index)
	err := http.ListenAndServe(":8000", nil)

	if err != nil {
		fmt.Println(err)
	}
}