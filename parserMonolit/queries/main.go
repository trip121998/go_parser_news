package main

import (
	"fmt"
	"queries/library"
)

var zakonNews []map[string]string
var tengryNews []map[string]string

func main() {
	fmt.Println("queries")
	zakonNews = library.SearchAllNewsZakon();
	fmt.Println("\n\n\n", zakonNews)
	tengryNews = library.SearchAllNewsTengry();
	fmt.Println("\n\n\n", tengryNews);
	nurNews := library.SearchAllNewsNur();
	fmt.Println("\n\n\n", nurNews);
}