package library

import (
	"fmt"
	"regexp"
	"strconv"
	"time"
)

func SearchAllNewsZakon() (news []map[string]string) {
	fmt.Println(time.Now());
	url := "https://www.zakon.kz/search?qsearch=&author=&category=&tag=&perioddate=2024.01.24+-+2024.01.24";
	text := queryZakon(url, 0);
	parseValueToFile(text);

	paginationRegexp := regexp.MustCompile(`(?ms)href="\/search\/\?qsearch=&amp;author=&amp;category=&amp;tag=&amp;perioddate=.*?p=(?P<pagination>[0-9]+)"`)
	searchValue := paginationRegexp.FindAllStringSubmatch(text, -1);
    result := installOneMatchSubexpNames(paginationRegexp, searchValue[len(searchValue)-1])

    pagination, err := strconv.Atoi(result["pagination"])
    if err != nil {
        panic(err)
    }

    var count = pagination * 20;
	news = make([]map[string]string, 0, count);
	buffer := findNewsZakon(text)
	news = append(news, buffer...);

    for i := 2; i <= pagination; i++ {
    	text := queryZakon(url, i);
    	buffer := findNewsZakon(text)
		news = append(news, buffer...);
    }

    news = reverseSlice(news);

	fmt.Println(time.Now());
	return;
}

func findNewsZakon(text string) (result []map[string]string) {
	regexpString := `(?ms)zmainCard_item[ ]+card_md.*?href="(?P<link>.*?)".*?<div class="title">(?P<title>.*?)<\/div>.*?<time.*?datetime="(?P<datetime>.*?)".*?<\/a>|paginationWrap`;
	buffer := findNews(text, regexpString);

	for _, v := range buffer {
		result = append(result, v);
	}

	return;
}

func queryZakon(url string, pagination int) string {
	if (pagination != 0) {
		url = fmt.Sprintf(url+"&p=%d", pagination);
		return request(url, false);
	} else {
		return request(url, false);
	}

}