package library

import (
	"fmt"
	"time"
	"regexp"
	"strconv"
	"encoding/json"
	"golang.org/x/net/html"
)

func SearchAllNewsTengry() (news []map[string]string) {
	fmt.Println("tengry")
	fmt.Println(time.Now());
	url := "https://tengrinews.kz/search/%s?text=&date_start=2024-01-24&date_end=2024-01-24&field=all"
	pageRegexpString := `(?ms)href="/search/page/[0-9]+/\?field=all.*?>(?P<page>[0-9]+)<\/a><\/li>`;

	text := queryTengry(url, 0);
	parseValueToFile(text);

	pageRegexp := regexp.MustCompile(pageRegexpString)
	searchValue := pageRegexp.FindAllStringSubmatch(text, -1);
    result := installOneMatchSubexpNames(pageRegexp, searchValue[len(searchValue)-1])

    endPage, err := strconv.Atoi(result["page"])
    if err != nil {
        panic(err)
    }

    var count = endPage * 20;
	news = make([]map[string]string, 0, count);
	buffer := findNewsTengry(text);
	news = append(news, buffer...);
    
    for i := 2; i <= endPage; i++ {
    	text := queryTengry(url, i);
    	buffer := findNewsTengry(text)
		news = append(news, buffer...);
    }

    news = reverseSlice(news);

	fmt.Println(time.Now());
	return news;
}

func findNewsTengry(text string) (result []map[string]string) {
	regexpString := `(?ms)tn-search-news-list-item.*?href="(?P<link>.*?)">(?P<title>.*?)<\/a>.*?<time>(?P<date>.*?)</time>.*?data-id="(?P<id>.*?)".*?div|tn-search-news-list-item-image`;
	buffer := findNews(text, regexpString);

	for i := 0; i < len(buffer)-1; i++ {
		dateTime := getDateTimeTengry(buffer[i]["date"]);
		id, _ := strconv.Atoi(buffer[i]["id"]);
		views := getViewsTengry(id, buffer[i]["link"]);

		buffer[i]["date"] = dateTime["date"];
		buffer[i]["time"] = dateTime["time"];
		buffer[i]["title"] = html.UnescapeString(buffer[i]["title"]);
		buffer[i]["views"] = views;
	}

	for _, v := range buffer {
		result = append(result, v);
	}

	return;
}

func queryTengry(url string, page int) string {
	if (page != 0) {
		url = fmt.Sprintf(url, fmt.Sprintf("page/%d/", page));
		return request(url, false);
	} else {
		url = fmt.Sprintf(url, "");
		return request(url, false);
	}
}

func getDateTimeTengry(val string) (result map[string]string) {
	for k, v := range months {
		monthRegexp := regexp.MustCompile(k);
		match := monthRegexp.MatchString(val);
		if (match) {
			replaceFormatDate := monthRegexp.ReplaceAllString(val, fmt.Sprintf("$1-%s-$3 $4", v));
			dateTimeRegexp := regexp.MustCompile("(?P<date>[0-9]+.*?[0-9]+.*?[0-9]+.*?(?P<time>[0-9]+:[0-9]+))");

			searchValue := dateTimeRegexp.FindStringSubmatch(replaceFormatDate);
			result = installOneMatchSubexpNames(dateTimeRegexp, searchValue);
			break;
		}
	}
	return
}

func getViewsTengry(id int, link string) string {
	prefix := "tengrinews_ru";
	linkRegexp := regexp.MustCompile(`travel`)
	if (linkRegexp.MatchString(link)) {
		prefix = "tengritravel_ru";
	}

	url := fmt.Sprintf("https://counter.tengrinews.kz/inc/%s/news/%d", prefix, id);
	request := request(url, false)

	var buffer map[string]int
	err := json.Unmarshal([]byte(request), &buffer)
	if err != nil {
		fmt.Println("error:", err)
	}

	return strconv.Itoa(buffer["result"]);
}