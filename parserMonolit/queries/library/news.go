package library

import (
	"regexp"
)


func findNews(text string, regexpString string) (buffer []map[string]string) {
	linksRegexp := regexp.MustCompile(regexpString)
	searchValue := linksRegexp.FindAllStringSubmatch(text, -1);
	buffer = installAllMatchSubexpNames(linksRegexp, searchValue);
	return
}