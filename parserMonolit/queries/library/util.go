package library

import (
	"fmt"
	"os"
	"time"
	"net/http"
	"regexp"
	"io/ioutil"
)

func parseValueToFile(text string) {
    file, err := os.Create("hello.txt")
     
    if err != nil{
        fmt.Println("Unable to create file:", err) 
        os.Exit(1) 
    }
    defer file.Close() 
    file.WriteString(text)
     
    fmt.Println("Done parse value to file.")
}

func reverseSlice(array []map[string]string) []map[string]string {
	for i := 0; i < len(array)/2; i++ {
		j := len(array) - i - 1
		array[i], array[j] = array[j], array[i]
	}
	return array
}

func installOneMatchSubexpNames(re *regexp.Regexp, searchValue []string) map[string]string {
    result := make(map[string]string)
 
    for i, name := range re.SubexpNames() {
        if i != 0 && name != "" {
            result[name] = searchValue[i]
        }
    }

    return result;
}

func installAllMatchSubexpNames(re *regexp.Regexp, searchValue [][]string) []map[string]string {
    result := make([]map[string]string, len(searchValue)-1, len(searchValue)-1)

 	for j := 0; j < len(searchValue)-1; j++ {
 		buffer := make(map[string]string);
	    for i, name := range re.SubexpNames() {
	        if i != 0 && name != "" {
	            buffer[name] = searchValue[j][i]
	        }
	    }
	    result[j] = buffer
	}

	return result;
}

func request(url string, ajax bool) string {
	method := "GET"

	client := &http.Client {
	}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		fmt.Println(err)
		return ""
	}
	req.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:121.0) Gecko/20100101 Firefox/121.0")
	if (ajax) {
		req.Header.Add("Accept", "application/json")
		req.Header.Add("X-Requested-With", "XMLHttpRequest")
	} else {
		req.Header.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8")		
	}
	req.Header.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3")
	req.Header.Add("Connection", "keep-alive")
	req.Header.Add("Upgrade-Insecure-Requests", "1")
	req.Header.Add("Sec-Fetch-Dest", "document")
	req.Header.Add("Sec-Fetch-Mode", "navigate")
	req.Header.Add("Sec-Fetch-Site", "same-origin")
	req.Header.Add("TE", "trailers")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return ""
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return ""
	}
	return string(body);
}

func trim(value string) string {
	trimRegexp := regexp.MustCompile(`(?ms)^(\n| +)(.*?)`);
	result := trimRegexp.ReplaceAllString(value, "$2");
	trimRegexp = regexp.MustCompile(`(?ms)(.*?) +$`);
	result = trimRegexp.ReplaceAllString(result, "$1");
	return result;
}

func strtotime(str string) (int64, error) {
	layout := "2006-01-02 15:04:05"
	t, err := time.Parse(layout, str)
	if err != nil {
		return 0, err
	}
	return t.Unix(), nil
}