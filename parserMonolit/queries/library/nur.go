package library

import (
	"fmt"
	"time"
	"regexp"
)

func SearchAllNewsNur() (news []map[string]string) {
	fmt.Println("Nur");
	fmt.Println(time.Now());
	url := "https://www.nur.kz/ajax/pagination/pseudo-category/latest/%d/";

	news = make([]map[string]string, 0, 0);
	var i = 1;

    for {
    	text := queryNur(url, i);
    	buffer, stop := findNewsNur(text)
    	if (stop) {
    		break;
    	}
		news = append(news, buffer...);
    	i++;
    }
    news = reverseSlice(news);

	fmt.Println(time.Now());
	return
}

func findNewsNur(text string) (result []map[string]string, stop bool) {
	regexpString := `(?ms)href="(?P<link>.*?)".*?datetime=(?P<datetime>.*?)\>.*?h2.*?>(?P<title>.*?)<\/h2>`;
	buffer := findNews(text, regexpString);
	stop = false;

	for _, v := range buffer {
		v["title"] = trim(v["title"]);
		v["link"] = trim(v["link"]);
		datetime := formateDateTime(v["datetime"]);
		v["datetime"] = fmt.Sprintf("%s %s", datetime["date"], datetime["time"]);
		
		if (!("2024-01-24" >= datetime["date"])) {
			continue;
		}

		if ("2024-01-24" > datetime["date"]) {
			stop = true;
			break;
		}

		result = append(result, v);
	}

	return;
}

func formateDateTime(datetime string) (result map[string]string) {
	datetime = trim(datetime);
	dateTimeRegexp := regexp.MustCompile(`(?ms)(?P<date>.*?)T(?P<time>[0-9]+:[0-9]+).*`)
	replaceFormatDate := dateTimeRegexp.FindStringSubmatch(datetime)
	result = installOneMatchSubexpNames(dateTimeRegexp, replaceFormatDate);

	return;
}

func queryNur(url string, page int) string {
	url = fmt.Sprintf(url, page);
	return request(url, true);
}