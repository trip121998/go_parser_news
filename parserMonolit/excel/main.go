package main

import (
    "fmt"

    "github.com/xuri/excelize/v2"
)

func main() {
    f := excelize.NewFile()
    defer func() {
        if err := f.Close(); err != nil {
            fmt.Println(err)
        }
    }()
    // Create a new sheet.
    // index, err := f.NewSheet("Sheet2")
    // if err != nil {
    //     fmt.Println(err)
    //     return
    // }
    // Set value of a cell.
    f.SetColWidth("Sheet1", "A", "A", 20)
    f.SetCellValue("Sheet1", "A1", "Hello World!")

    // Save spreadsheet by the given path.
    if err := f.SaveAs("Book1.xlsx"); err != nil {
        fmt.Println(err)
    }
}