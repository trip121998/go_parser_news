package main
import (
	"fmt"
)

var array []map[string]string

func main() {
	testArr := []map[string]string{map[string]string{"name": "test1"},map[string]string{"name": "test2"}}

	array = append(array, map[string]string{"name": "normal"})
	fmt.Println(array);
	array = append(array, testArr...)
	fmt.Println(array);
}