package gui

type ElementGuiInterface interface {
	SetMinimumSize2(width, height int)
	SetMaximumSize2(width, height int)
	Move2(x, y int)
}

var comboBoxState = map[int]bool {
	2: true,
	0: false,
}