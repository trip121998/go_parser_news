package gui

import (
	"github.com/therecipe/qt/widgets"
	"github.com/therecipe/qt/gui"
	"parserNews/tools"
)

func setSizeAndPosition(element ElementGuiInterface, width, height, x, y int) {
	element.SetMinimumSize2(width, height)
	element.SetMaximumSize2(width, height)
	element.Move2(x, y)
}

func setOptionsImage(label *widgets.QLabel, image *gui.QPixmap) {
	label.SetPixmap(image)
	label.SetScaledContents(true)
	label.Hide()
}

func setOptionsMovie(label *widgets.QLabel, movie *gui.QMovie) {
	label.SetMovie(movie)
	movie.Start()
	label.Hide()
}

func setEnabledElements(button *widgets.QPushButton, enable bool) {
	button.SetEnabled(enable)

	for _, stateNews := range tools.StatesNews {
		stateNews.CheckBox.SetEnabled(enable)
	}
}