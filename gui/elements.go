package gui

import (
	"github.com/therecipe/qt/widgets"
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"parserNews/tools"
)

func addInput(parent *widgets.QWidget, y int) *widgets.QDateEdit {
	input := widgets.NewQDateEdit(parent);

	setSizeAndPosition(input, 171, 22, 15, y)
	input.SetCalendarPopup(true);
	input.SetDateTime(core.QDateTime_CurrentDateTime());

	return input
}

func addCheckBoxs(parent *widgets.QGroupBox, comboBox *widgets.QComboBox) /**widgets.QComboBox*/ {
	var (
		firstColumnCount = 0
		checkBoxFirstColumnX = 50
		imageFirstColumnX = 30

		secondColumnCount = 0
		checkBoxSecondColumnX = 160
		imageSecondColumnX = 140

		countElementsFromColumn = 3
		calculateColumnY = func(count int) int {
			return 40 + 30 * count;
		}
	)

	for keyStateNews, stateNews := range tools.StatesNews {
		checkBox := widgets.NewQCheckBox2(stateNews.Name, parent);
		var imagesCheckbox [3]*widgets.QLabel

		if keyStateNews < countElementsFromColumn {
			checkBox.Move2(checkBoxFirstColumnX, calculateColumnY(firstColumnCount));
			imagesCheckbox = setImagesToCheckbox(parent, imageFirstColumnX, calculateColumnY(firstColumnCount))
			firstColumnCount++
		} else {
			checkBox.Move2(checkBoxSecondColumnX, calculateColumnY(secondColumnCount));
			imagesCheckbox = setImagesToCheckbox(parent, imageSecondColumnX, calculateColumnY(secondColumnCount))
			secondColumnCount++
		}

		checkBox.ConnectStateChanged(func(state int) {
			checkBoxChanged(state, stateNews.Name, comboBox)
		})

		tools.StatesNews[keyStateNews].CheckBox = checkBox;
		tools.StatesNews[keyStateNews].SetImagesCheckbox(imagesCheckbox)
	}
}

func checkBoxChanged(state int, stateNewsName string, comboBox *widgets.QComboBox) {
	for _, v := range tools.StatesNews {
		v.CheckImageCheckbox(-1)
	}

	if stateNewsName == "Tengry" {
		comboBox.SetEnabled(comboBoxState[state])
	}
}

func setImagesToCheckbox(parent *widgets.QGroupBox, x, y int) [3]*widgets.QLabel {
	var (
		widthImage = 15
		heightImage = 15
	)

	loaderImage := gui.NewQMovie(parent)
	loaderImage.SetFileName("images/loader.gif")
	loaderImage.SetScaledSize(core.NewQSize2(widthImage, heightImage))

	successImage := gui.NewQPixmap3("images/success.png", "png", core.Qt__AutoColor)
	successImage = successImage.Scaled2(widthImage, heightImage, core.Qt__IgnoreAspectRatio, core.Qt__FastTransformation)

	failedImage := gui.NewQPixmap3("images/failed.png", "png", core.Qt__AutoColor)
	failedImage = failedImage.Scaled2(widthImage, heightImage, core.Qt__IgnoreAspectRatio, core.Qt__FastTransformation)

	labelPicture1 := widgets.NewQLabel(parent, core.Qt__Widget)
	setSizeAndPosition(labelPicture1, widthImage, heightImage, x, y)
	setOptionsMovie(labelPicture1, loaderImage)

	labelPicture2 := widgets.NewQLabel(parent, core.Qt__Widget)
	setSizeAndPosition(labelPicture2, widthImage, heightImage, x, y)
	setOptionsImage(labelPicture2, successImage)

	labelPicture3 := widgets.NewQLabel(parent, core.Qt__Widget)
	setSizeAndPosition(labelPicture3, widthImage, heightImage, x, y)
	setOptionsImage(labelPicture3, failedImage)

	return [3]*widgets.QLabel{labelPicture1, labelPicture2, labelPicture3}
}