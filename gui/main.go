package gui

import (
	"os"
	"fmt"
	"time"
	"strconv"
	"os/exec"
	"github.com/therecipe/qt/widgets"
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"parserNews/news"
	"parserNews/tools"
)





func init() {
	fmt.Println("Init");
	for _, v := range tools.NamesNews {
		tools.StatesNews = append(tools.StatesNews, tools.StateNews{
			Name: v,
			News: tools.NewsChan{},
			Columns: tools.ColumnsNews[v],
		});
	}
}

func Generate() {
	widgets.NewQApplication(len(os.Args), os.Args)
	var desktopKey = 0
	desktop := widgets.NewQDesktopWidget()
	desktopCenterPosition := desktop.AvailableGeometry(desktopKey).Center();

	var (
		windowWidth = 505
		windowHeight = 220

		windowX = desktopCenterPosition.X() - windowWidth
		windowY = desktopCenterPosition.Y() - windowHeight
	)

	window := widgets.NewQMainWindow(nil, 0)

	window.SetWindowIcon(gui.NewQIcon5("logo.png"))
	window.SetWindowTitle("Парсер")
	setSizeAndPosition(window, windowWidth, windowHeight, windowX, windowY)

	layout := widgets.NewQHBoxLayout()

	widget := widgets.NewQWidget(nil, 0)
	widget.SetLayout(layout)

	inputDateBegin := addInput(widget, 30);
	inputDateEnd := addInput(widget, 70);

	label := widgets.NewQLabel2("Tengry:", widget, core.Qt__Widget);
	label.Move2(15, 100);
	label.SetVisible(false)

	comboBox := widgets.NewQComboBox(widget);
	setSizeAndPosition(comboBox, 41, 22, 65, 105)
	comboBox.SetVisible(false)

	comboBox.AddItem("1", core.NewQVariant());
	comboBox.AddItem("2", core.NewQVariant());

	comboBox.SetCurrentIndex(1);
	comboBox.SetEnabled(false);

	button := widgets.NewQPushButton2("Сгенерировать", widget)
	setSizeAndPosition(button, 171, 41, 15, 120)

	button.ConnectClicked(func(checked bool) {
		ok := false
		for _, stateNews:= range tools.StatesNews {
			if stateNews.CheckBox.CheckState() == 2 {
				ok = true;
				stateNews.CheckImageCheckbox(tools.ImagesCheckboxKeys["progress"])
			} else {
				stateNews.CheckImageCheckbox(tools.ImagesCheckboxKeys["not"])
			}
		}

		if !ok {
			widgets.QMessageBox_Critical(
				nil, 
				"Ошибка", 
				"Выбери хотя бы один новостной сайт", 
				widgets.QMessageBox__Ok, 
				widgets.QMessageBox__Ok,
			);
			return;
		}

		dateBegin := inputDateBegin.Date().ToString("yyyy-MM-dd");
		dateEnd := inputDateEnd.Date().ToString("yyyy-MM-dd");
		tengryVersion, _ := strconv.Atoi(comboBox.CurrentText());
		fmt.Println(dateBegin, dateEnd, tengryVersion);

		setEnabledElements(button, false)
		
		go func() {
			f := news.GetExcelAllNews(dateBegin, dateEnd, tengryVersion)

			fileDialog := widgets.NewQFileDialog(widget, core.Qt__Dialog)
			options := fileDialog.Options()
			fileNameString := fmt.Sprintf("Отчет%s.xlsx", time.Now().Format("20060102150405"));
			fileName := fileDialog.GetSaveFileName(nil,"QFileDialog.getOpenFileNames()", fileNameString,"Excel Files (*.xlsx)", "", options)

			if fileName != "" {
				if err := f.SaveAs(fileName); err != nil {
					widgets.QMessageBox_Critical(nil, "Ошибка", err.Error(), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
			    }

				cmd := exec.Command("explorer", "")
				cmd.Run()
			}

			setEnabledElements(button, true)
		}()
	})

	groupBox := widgets.NewQGroupBox(widget);
	setSizeAndPosition(groupBox, 291, 171, 200, 10)
	addCheckBoxs(groupBox, comboBox)

	window.SetCentralWidget(widget)
	window.Show()

	widgets.QApplication_Exec()
}
