package news

import (
	"fmt"
	"time"
	"regexp"
	"unicode/utf8"
	"parserNews/tools"
	"parserNews/parserErrors"
)

type Nur struct {
	DateBegin string
	DateEnd string
	Chan chan tools.NewsChan
}

func NewNur(dateBegin, dateEnd string) *Nur {
	return &Nur {
		DateBegin: dateBegin,
		DateEnd: dateEnd,
		Chan: make(chan tools.NewsChan),
	}
}

func (nur Nur) GetChan() chan tools.NewsChan {
	return nur.Chan;
}

func (nur *Nur) SetChan(newChan chan tools.NewsChan) {
	nur.Chan = newChan
}

func (nur Nur) SearchAllNews(key int) {
	newsChan := tools.NewsChan{Key: key};
	fmt.Println("Nur----------", time.Now());
	url := "https://www.nur.kz/ajax/pagination/pseudo-category/latest/%d/";

	news := make([]map[string]string, 0, 0);
	var i = 1;

    for {
    	text, err := nur.query(url, i);
    	if err != nil {
    		newsChan.Err = err;
    		nur.Chan <- newsChan;
    		return;
    	}
    	buffer, stop, err := nur.findNews(text)
    	if err != nil {
    		newsChan.Err = err;
    		nur.Chan <- newsChan;
    		return;
    	}

		news = append(news, buffer...);

    	if stop {
    		break;
    	}

    	i++;
    }
    news = tools.ReverseSlice(news);

	fmt.Println("Nur----------", time.Now());
	newsChan.Value = news;
	nur.Chan <- newsChan;
}

func (nur Nur) findNews(text string) (result []map[string]string, stop bool, err error) {
	regexpString := `(?ms)href="(?P<link>.*?)".*?datetime=(?P<datetime>.*?)\>.*?h2.*?>(?P<title>.*?)<\/h2>`;
	buffer := findNews(text, regexpString);
	stop = false;

	for _, v := range buffer {
		v["title"] = tools.Trim(v["title"]);
		v["link"] = tools.Trim(v["link"]);
		datetime := nur.formateDateTime(v["datetime"]);
		if len(datetime) == 0 {
			err = parserErrors.ERROR_NUR_DATETIME_NOT_CORRECT;
			return;
		}

		v["characters"] = fmt.Sprintf("%d", utf8.RuneCountInString(v["title"]));
		v["date"] = fmt.Sprintf("%s %s", datetime["date"], datetime["time"]);
		v["time"] = datetime["time"];

		if !(nur.DateEnd >= datetime["date"]) {
			continue;
		}

		if nur.DateBegin > datetime["date"] {
			stop = true;
			break;
		}

		result = append(result, v);
	}

	return;
}

func (nur Nur) formateDateTime(datetime string) (result map[string]string) {
	datetime = tools.Trim(datetime);
	dateTimeRegexp := regexp.MustCompile(`(?ms)(?P<date>.*?)T(?P<time>[0-9]+:[0-9]+).*`)
	replaceFormatDate := dateTimeRegexp.FindStringSubmatch(datetime)
	result = tools.InstallOneMatchSubexpNames(dateTimeRegexp, replaceFormatDate);

	return;
}

func (nur Nur) query(url string, page int) (string, error) {
	url = fmt.Sprintf(url, page);
	return tools.Request(url, true);
}