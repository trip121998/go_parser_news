package news

import (
	"fmt"
	"time"
	"regexp"
	"strconv"
	"unicode/utf8"
	"encoding/json"
	"parserNews/tools"
	"golang.org/x/net/html"
	"parserNews/parserErrors"
)

type Tengry2 struct {
	DateBegin string
	DateEnd string
	Chan chan tools.NewsChan
}

func NewTengry2(dateBegin, dateEnd string) *Tengry2 {
	return &Tengry2 {
		DateBegin: dateBegin,
		DateEnd: dateEnd,
		Chan: make(chan tools.NewsChan),
	}
}

func (tengry Tengry2) GetChan() chan tools.NewsChan {
	return tengry.Chan;
}

func (tengry *Tengry2) SetChan(newChan chan tools.NewsChan) {
	tengry.Chan = newChan
}

func (tengry Tengry2) SearchAllNews(key int) {
	newsChan := tools.NewsChan{Key: key};
	fmt.Println("Tengry----------", time.Now());
	url := "https://tengrinews.kz/search/%s?text=&date_start=%s&date_end=%s&field=all"
	text, err := tengry.query(url, 0);

	if err != nil {
		newsChan.Err = err;
		tengry.Chan <- newsChan;
		return;
	}

	endPage, countPage, err := tengry.getPages(text);
	if err != nil {
		newsChan.Err = err;
		tengry.Chan <- newsChan;
		return;
	}

	news := make([]map[string]string, 0, countPage);
	buffer, err := tengry.findNews(text);

	if err != nil {
		newsChan.Err = err;
		tengry.Chan <- newsChan;
		return;
	}

	news = append(news, buffer...);

	if endPage == 1 {
	    news = tools.ReverseSlice(news);
		newsChan.Value = news;
		tengry.Chan <- newsChan;
		return
	}
    
    for i := 2; i <= endPage; i++ {
    	text, err := tengry.query(url, i);
		if err != nil {
			newsChan.Err = err;
			tengry.Chan <- newsChan;
			return;
		}
    	buffer, err := tengry.findNews(text)
		if err != nil {
			newsChan.Err = err;
			tengry.Chan <- newsChan;
			return;
		}
		if buffer == nil {
			continue;
		}

		news = append(news, buffer...);
    }

    news = tools.ReverseSlice(news);

	fmt.Println("Tengry----------", time.Now());
	newsChan.Value = news;
	tengry.Chan <- newsChan;
}

func (tengry Tengry2) getPages(text string) (endPage, countPage int, err error) {
	var bufferEndPage string

	pageRegexpString := `(?ms)href="/search/page/[0-9]+/\?field=all.*?>(?P<page>[0-9]+)<\/a><\/li>`;
	pageRegexp := regexp.MustCompile(pageRegexpString)
	searchValue := pageRegexp.FindAllStringSubmatch(text, -1);

	if len(searchValue) != 0 {
	    result := tools.InstallOneMatchSubexpNames(pageRegexp, searchValue[len(searchValue)-1])

	    value, ok := result["page"];
	    if !ok {
	    	err = parserErrors.ERROR_TENGRY_NOT_PAGE;
	    	return;
	    } else {
	    	bufferEndPage = value;
	    }
	} else {
		bufferEndPage = "1";
	}

    endPage, err = strconv.Atoi(bufferEndPage)
    if err != nil {
        return;
    }

    if endPage == 0 {
    	err = parserErrors.ERROR_TENGRY_NOT_PAGE;
    	return;
    }

    countPage = endPage * 20;
    return;
}

func (tengry Tengry2) findNews(text string) (result []map[string]string, err error) {
	regexpLinks := regexp.MustCompile(`(?ms)content_main_item".*?href="(?P<link>.*?)"`);
	searchValue := regexpLinks.FindAllStringSubmatch(text, -1);
	links := tools.InstallAllMatchSubexpNames(regexpLinks, searchValue);


	for _, v := range links {
		regexpHTTP := regexp.MustCompile(`http`);
		if !regexpHTTP.MatchString(v["link"]) {
			v["link"] = "https://tengrinews.kz"+v["link"]
		}

		values, err := tengry.getContent(v["link"]);
		if err != nil {
			continue;
		}

		dateTime := tengry.getDateTime(values["datetime"]);
		if len(dateTime) == 0 {
			err = parserErrors.ERROR_TENGRY_DATETIME_NOT_CORRECT;
			return result, err;
		}

		id, err := strconv.Atoi(values["id"]);
		if err != nil {
			return result, err;
		}

		views, err := tengry.getViews(id, values["link"]);
		if err != nil {
			return result, err;
		}

		values["date"] = dateTime["date"];
		values["time"] = dateTime["time"];
		values["views"] = views;
		values["title"] = html.UnescapeString(values["title"]);
		values["characters"] = fmt.Sprintf("%d", utf8.RuneCountInString(values["title"]));

		result = append(result, values);
	}

	return;
}

func (tengry Tengry2) getContent(link string) (result map[string]string, err error) {
	request, err := tools.Request(link, false)
	if err != nil {
		return result, err;
	}

	regexpStrings := []string{
		`(?ms)date-time.*?>(?P<datetime>.*?)<.*?<h1.*?>(?P<title>.*?)<.*?data-id="(?P<id>.*?)"`,
		`(?ms)entry-title">(?P<title>.*?)<\/h1>.*?entry-date.*?">(?P<datetime>.*?)<.*?data-id="(?P<id>.*?)"`,
		`(?ms)post-title">(?P<title>.*?)<.*?date">(?P<datetime>.*?)<.*?data-id="(?P<id>.*?)"`,
	}

	var regexpValues *regexp.Regexp;

	for _, v := range regexpStrings {
		regexpBuffer := regexp.MustCompile(v);
		if regexpBuffer.MatchString(request) {
			regexpValues = regexpBuffer;
		}
	}
	if regexpValues == nil {
		err = parserErrors.ERROR_TENGRY_NOT_FIND_REGEXP_CONTENT
		return;
	}

	searchValue := regexpValues.FindStringSubmatch(request);
	if len(searchValue) == 0 {
		err = parserErrors.ERROR_TENGRY_NOT_FIND_CONTENT_REGEXP;
		return;
	}
	result = tools.InstallOneMatchSubexpNames(regexpValues, searchValue);
	result["link"] = link;

	return;
}

func (tengry Tengry2) query(url string, page int) (string, error) {
	if page != 0 {
		url = fmt.Sprintf(url, fmt.Sprintf("page/%d/", page), tengry.DateBegin, tengry.DateEnd);
		return tools.Request(url, false);
	} else {
		url = fmt.Sprintf(url, "", tengry.DateBegin, tengry.DateEnd);
		return tools.Request(url, false);
	}
}

func (tengry Tengry2) getDateTime(val string) (result map[string]string) {
	for k, v := range tools.MonthsDateTime {
		monthRegexp := regexp.MustCompile(k);
		match := monthRegexp.MatchString(val);
		if match {
			replaceFormatDate := monthRegexp.ReplaceAllString(val, fmt.Sprintf("$3-%s-$1 $4", v));
			dateTimeRegexp := regexp.MustCompile("(?P<date>[0-9]+.*?[0-9]+.*?[0-9]+.*?(?P<time>[0-9]+:[0-9]+))");

			searchValue := dateTimeRegexp.FindStringSubmatch(replaceFormatDate);
			result = tools.InstallOneMatchSubexpNames(dateTimeRegexp, searchValue);
			break;
		}
	}

	return
}

func (tengry Tengry2) getViews(id int, link string) (string, error) {
	prefix := "tengrinews_ru";
	linkRegexp := regexp.MustCompile(`travel`)
	if linkRegexp.MatchString(link) {
		prefix = "tengritravel_ru";
	}

	url := fmt.Sprintf("https://counter.tengrinews.kz/inc/%s/news/%d", prefix, id);
	request, err := tools.Request(url, false)
	if err != nil {
		return "", err;
	}

	var buffer map[string]int
	err = json.Unmarshal([]byte(request), &buffer)
	if err != nil {
		fmt.Println("error:", err)
		return "", err;
	}

	return strconv.Itoa(buffer["result"]), nil;
}