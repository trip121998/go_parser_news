package news

import (
	"fmt"
	"regexp"
	"strconv"
	"time"
	"unicode/utf8"
	"parserNews/tools"
	"parserNews/parserErrors"
)

type Zakon struct {
	DateBegin string
	DateEnd string
	Chan chan tools.NewsChan
}

func NewZakon(dateBegin, dateEnd string) *Zakon {
	return &Zakon {
		DateBegin: dateBegin,
		DateEnd: dateEnd,
		Chan: make(chan tools.NewsChan),
	}
}

func (zakon Zakon) GetChan() chan tools.NewsChan {
	return zakon.Chan;
}

func (zakon *Zakon) SetChan(newChan chan tools.NewsChan) {
	zakon.Chan = newChan
}

func (zakon Zakon) SearchAllNews(key int)  {
	newsChan := tools.NewsChan{Key: key};
	fmt.Println("Zakon----------", time.Now());

	// DateBeginBuffer, err := zakon.formatDate(zakon.DateBegin);
	// if err != nil {
	// 	newsChan.Err = err;
	// 	zakon.Chan <- newsChan;
	// 	return;
	// }
	// zakon.DateBegin = DateBeginBuffer;

	// DateEndBuffer, err := zakon.formatDate(zakon.DateEnd);
	// if err != nil {
	// 	newsChan.Err = err;
	// 	zakon.Chan <- newsChan;
	// 	return;
	// }
	// zakon.DateEnd = DateEndBuffer;

	url := "https://www.zakon.kz/search?qsearch=&author=&category=&tag=&perioddate=%s+-+%s";
	text, err := zakon.query(url, 0);
	if err != nil {
		newsChan.Err = err;
		zakon.Chan <- newsChan;
		return;
	}

	var bufferEndPage string
	pageRegexp := regexp.MustCompile(`(?ms)href="\/search\/\?qsearch=&amp;author=&amp;category=&amp;tag=&amp;perioddate=.*?p=(?P<page>[0-9]+)"`)
	searchValue := pageRegexp.FindAllStringSubmatch(text, -1);
	if len(searchValue) != 0 {
	    result := tools.InstallOneMatchSubexpNames(pageRegexp, searchValue[len(searchValue)-1])

	    value, ok := result["page"];
	    if !ok {
	    	newsChan.Err = parserErrors.ERROR_ZAKON_NOT_PAGE;
	    	zakon.Chan <- newsChan;
	    	return;
	    } else {
	    	bufferEndPage = value;
	    }
	} else {
		bufferEndPage = "1";
	}

    endPage, err := strconv.Atoi(bufferEndPage)
    if err != nil {
        newsChan.Err = err;
        zakon.Chan <- newsChan;
        return;
    }

    var count = endPage * 20;
	news := make([]map[string]string, 0, count);
	buffer := zakon.findNews(text)
	news = append(news, buffer...);

	if endPage == 1 {
	    news = tools.ReverseSlice(news);
		newsChan.Value = news;
		zakon.Chan <- newsChan;
		return;
	}

    for i := 2; i <= endPage; i++ {
    	text, err := zakon.query(url, i);
    	if err != nil {
    		newsChan.Err = err;
    		zakon.Chan <- newsChan;
    		return;
    	}
    	buffer := zakon.findNews(text)
		news = append(news, buffer...);
    }

    news = tools.ReverseSlice(news);
    fmt.Println("news", len(news))
	fmt.Println("Zakon----------", time.Now());
	newsChan.Value = news;
	zakon.Chan <- newsChan;
}

func (zakon Zakon) formatDate(date string) (string, error) {
	dateRegexp := regexp.MustCompile(`(\d+).(\d+).(\d+)`);
	match := dateRegexp.MatchString(date);
	if match {
		return dateRegexp.ReplaceAllString(date, "$1.$2.$3"), nil;
	} else {
		return "", parserErrors.ERROR_ZAKON_DATE_FORMAT_NOT_CORRECT;
	}
}

func (zakon Zakon) findNews(text string) (result []map[string]string) {
	regexpString := `(?ms)zmainCard_item[ ]+card_md.*?href="(?P<link>.*?)".*?<div class="title">(?P<title>.*?)<\/div>.*?<time.*?datetime="(?P<date>.*? (?P<time>.*?))".*?<\/a>|paginationWrap`;
	buffer := findNews(text, regexpString);

	for i := 0; i < len(buffer); i++ {
		date, _ := time.Parse("2006-01-02 15:04", buffer[i]["date"])
		dateString := date.Format("2006-01-02")

		if dateString < zakon.DateBegin {
			break
		}

		buffer[i]["link"] = fmt.Sprintf("https://zakon.kz%s", buffer[i]["link"]);
		buffer[i]["characters"] = fmt.Sprintf("%d", utf8.RuneCountInString(buffer[i]["title"]));
		result = append(result, buffer[i])
	}

	return;
}

func (zakon Zakon) query(url string, pagination int) (string, error) {
	dateBegin, _ := time.Parse("2006-01-02", zakon.DateBegin)
	dateEnd, _ := time.Parse("2006-01-02", zakon.DateEnd)
	dateBegin = dateBegin.AddDate(0,0,-1)

	if pagination != 0 {
		url = fmt.Sprintf(url+"&p=%d", dateBegin.Format("2006.01.02"), dateEnd.Format("2006.01.02"), pagination);
		return tools.Request(url, false);
	} else {
		url = fmt.Sprintf(url, dateBegin.Format("2006.01.02"), dateEnd.Format("2006.01.02"));
		return tools.Request(url, false);
	}

}