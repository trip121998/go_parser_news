package news

import (
	"fmt"
	"time"
	"regexp"
	"unicode/utf8"
	"parserNews/tools"
	"golang.org/x/net/html"
	"strconv"
)

type Vesti struct {
	DateBegin string
	DateEnd string
	Chan chan tools.NewsChan
}

func NewVesti(dateBegin, dateEnd string) *Vesti {
	return &Vesti {
		DateBegin: dateBegin,
		DateEnd: dateEnd,
		Chan: make(chan tools.NewsChan),
	}
}

func (vesti Vesti) GetChan() chan tools.NewsChan {
	return vesti.Chan;
}

func (vesti *Vesti) SetChan(newChan chan tools.NewsChan) {
	vesti.Chan = newChan
}

var sites []string = []string{
	"boxing",
	"mixedfights",
	"football",
	"hockey",
	"mini",
	"tennis",
	"wrestling",
	"athletic_sports",
	"basketball",
	"other",
	"lifestyle",
}

func (vesti Vesti) SearchAllNews(key int) {
	newsChans := make(chan tools.NewsChan, len(sites));
	var newsChan = tools.NewsChan{Key: key}
	for _, v := range sites {
		url := fmt.Sprintf("https://vesti.kz/%s/archive/page/", v)+"%d";
		go vesti.SearchUrlNews(url, newsChans);

	}
	news := make([]map[string]string, 0, 0);
	for i := 0; i < len(sites); i++ {	
		newsChan = <- newsChans;
		if newsChan.Err != nil {
			vesti.Chan <- newsChan
		}
		news = append(news, newsChan.Value...);
	}
	// fmt.Println(len(news))
	newsChan.Value = news;
	vesti.Chan <- newsChan;
}

func (vesti Vesti) SearchUrlNews(url string, newsChans chan tools.NewsChan) {
	newsChan := tools.NewsChan{};
	fmt.Println("Begin Vesti----------",url, time.Now());

	text, err := vesti.query(url, 0);
	if err != nil {
		newsChan.Err = err;
		newsChans <- newsChan;
		return;
	}

	var bufferEndPage string
	pageRegexpString := `(?ms)\/page\/\d+">(?P<page>\d+)<`;
	pageRegexp := regexp.MustCompile(pageRegexpString)
	searchValue := pageRegexp.FindAllStringSubmatch(text, -1);
	if len(searchValue) != 0 {
	    result := tools.InstallOneMatchSubexpNames(pageRegexp, searchValue[len(searchValue)-1])

	    value, ok := result["page"];
	    if !ok {
	    	newsChan.Err = fmt.Errorf("no page");
	    	newsChans <- newsChan;
	    	return;
	    } else {
	    	bufferEndPage = value;
	    }
	} else {
		bufferEndPage = "1";
	}

    endPage, err := strconv.Atoi(bufferEndPage)
    if err != nil {
        newsChan.Err = err;
        newsChans <- newsChan;
        return;
    }

    if endPage == 0 {
    	newsChan.Err = fmt.Errorf("no pages");
    	newsChans <- newsChan;
    	return;
    }
    
	news := make([]map[string]string, 0, 0);
	buffer, stop := vesti.findNews(text);
	news = append(news, buffer...);

	var i = 2;

	if !stop {
	    for {
	    	text, err := vesti.query(url, i);
	    	if err != nil {
	    		newsChan.Err = err;
	    		newsChans <- newsChan;
	    		return;
	    	}

	    	buffer, stop := vesti.findNews(text)
			news = append(news, buffer...);

			if stop {
				break;
			}

	    	i++;
	    }
	}
    news = tools.ReverseSlice(news);
	fmt.Println("End Vesti----------", url, time.Now(), len(news));
	newsChan.Value = news;
	newsChans <- newsChan;
}

func (vesti Vesti) findNews(text string) (result []map[string]string, stop bool) {
	regexpString := `(?ms)"item ".*?d">(?P<date>(.*? +.*?))<.*?h">.*?href="(?P<link>.*?)">(?P<title>.*?)<|pagination`;
	buffer := findNews(text, regexpString);

	for i := 0; i < len(buffer); i++ {
		if (buffer[i]["title"] == "") {
			continue;
		}

		dateTime := vesti.getDateTime(buffer[i]["date"]);
		if !(vesti.DateEnd >= dateTime["date"]) {
			continue;
		}
		if vesti.DateBegin > dateTime["date"] {
			stop = true;
			break;
		}
		buffer[i]["date"] = dateTime["date"];
		buffer[i]["time"] = dateTime["time"];
		buffer[i]["title"] = tools.Trim(buffer[i]["title"]);
		buffer[i]["title"] = html.UnescapeString(buffer[i]["title"]);
		buffer[i]["link"] = fmt.Sprintf("https://vesti.kz%s", buffer[i]["link"]);
		buffer[i]["characters"] = fmt.Sprintf("%d", utf8.RuneCountInString(buffer[i]["title"]));

		result = append(result, buffer[i]);
	}

	return;
}

func (vesti Vesti) query(url string, page int) (string, error) {
	url = fmt.Sprintf(url, page);	
	return tools.Request(url, true);
}

func (vesti Vesti) getDateTime(val string) (result map[string]string) {
	for k, v := range tools.DatesVesti {
		monthRegexp := regexp.MustCompile(k);
		match := monthRegexp.MatchString(val);
		if match {
			replaceFormatDate := monthRegexp.ReplaceAllString(val, fmt.Sprintf("$3-%s-$1 $4", v));
			yearsRegexp := regexp.MustCompile(`^ -`);
			if yearsRegexp.MatchString(replaceFormatDate) {
				replaceFormatDate = yearsRegexp.ReplaceAllString(replaceFormatDate, strconv.Itoa(time.Now().Year())+"-");
			}

			monthDoubleCharRegexp := regexp.MustCompile(`(\d{4}-\d{2}-)(\d{2})`)
			if !monthDoubleCharRegexp.MatchString(replaceFormatDate) {
				monthDoubleCharRegexp = regexp.MustCompile(`(\d{4}-\d{2}).*?(\d)`);
				replaceFormatDate = monthDoubleCharRegexp.ReplaceAllString(replaceFormatDate, "$1-0$2")
			}

			dateTimeRegexp := regexp.MustCompile("((?P<date>[0-9]+.*?[0-9]+.*?[0-9]+).*?(?P<time>[0-9]+:[0-9]+))");

			searchValue := dateTimeRegexp.FindStringSubmatch(replaceFormatDate);
			result = tools.InstallOneMatchSubexpNames(dateTimeRegexp, searchValue);
			break;
		}
	}

	return
}