package news

import (
	"fmt"
	"time"
	"regexp"
	"strconv"
	"unicode/utf8"
	"encoding/json"
	"parserNews/tools"
	"golang.org/x/net/html"
)

type TengryInterface interface {
	SearchAllNews()
	GetChan() chan tools.NewsChan
}

type Tengry struct {
	DateBegin string
	DateEnd string
	Chan chan tools.NewsChan
}


func NewTengry(dateBegin, dateEnd string) *Tengry {
	return &Tengry {
		DateBegin: dateBegin,
		DateEnd: dateEnd,
		Chan: make(chan tools.NewsChan),
	}
}

func (tengry Tengry) GetChan() chan tools.NewsChan {
	return tengry.Chan;
}

func (tengry *Tengry) SetChan(newChan chan tools.NewsChan) {
	tengry.Chan = newChan
}

func (tengry Tengry) SearchAllNews(key int) {
	newsChan := tools.NewsChan{};
	fmt.Println("Tengry----------", time.Now());
	url := "https://tengrinews.kz/search/%s?text=&date_start=%s&date_end=%s&field=all"
	text, err := tengry.query(url, 0);

	if err != nil {
		newsChan.Err = err;
		tengry.Chan <- newsChan;
		return;
	}

	var bufferEndPage string
	pageRegexpString := `(?ms)href="/search/page/[0-9]+/\?field=all.*?>(?P<page>[0-9]+)<\/a><\/li>`;
	pageRegexp := regexp.MustCompile(pageRegexpString)
	searchValue := pageRegexp.FindAllStringSubmatch(text, -1);
	if len(searchValue) != 0 {
	    result := tools.InstallOneMatchSubexpNames(pageRegexp, searchValue[len(searchValue)-1])

	    value, ok := result["page"];
	    if !ok {
	    	newsChan.Err = fmt.Errorf("no page");
	    	tengry.Chan <- newsChan;
	    	return;
	    } else {
	    	bufferEndPage = value;
	    }
	} else {
		bufferEndPage = "1";
	}

    endPage, err := strconv.Atoi(bufferEndPage)
    if err != nil {
        newsChan.Err = err;
        tengry.Chan <- newsChan;
        return;
    }

    if endPage == 0 {
    	newsChan.Err = fmt.Errorf("no pages");
    	tengry.Chan <- newsChan;
    	return;
    }

    var count = endPage * 20;
	news := make([]map[string]string, 0, count);
	buffer, err := tengry.findNews(text);
	if err != nil {
		newsChan.Err = err;
		tengry.Chan <- newsChan;
		return;
	}
	news = append(news, buffer...);
	if endPage == 1 {
	    news = tools.ReverseSlice(news);
		newsChan.Value = news;
		tengry.Chan <- newsChan;
		return
	}
    
    for i := 2; i <= endPage; i++ {
    	text, err := tengry.query(url, i);
		if err != nil {
			newsChan.Err = err;
			tengry.Chan <- newsChan;
			return;
		}
    	buffer, err := tengry.findNews(text)
		if err != nil {
			newsChan.Err = err;
			tengry.Chan <- newsChan;
			return;
		}
		news = append(news, buffer...);
    }

    news = tools.ReverseSlice(news);

	fmt.Println("Tengry----------", time.Now());
	newsChan.Value = news;
	tengry.Chan <- newsChan;
}

func (tengry Tengry) findNews(text string) (result []map[string]string, err error) {
	regexpString := `(?ms)tn-search-news-list-item.*?href="(?P<link>.*?)">(?P<title>.*?)<\/a>.*?<time>(?P<date>.*?)</time>.*?data-id="(?P<id>.*?)".*?div|tn-search-news-list-item-image`;
	buffer := findNews(text, regexpString);

	for i := 0; i < len(buffer); i++ {
		dateTime := tengry.getDateTime(buffer[i]["date"]);
		if len(dateTime) == 0 {
			fmt.Println(dateTime, buffer[i]);
			err = fmt.Errorf("TENGRY: datetime not correct");
			return;
		}

		id, err := strconv.Atoi(buffer[i]["id"]);
		if err != nil {
			return result, err;
		}

		views, err := tengry.getViews(id, buffer[i]["link"]);
		if err != nil {
			return result, err;
		}

		buffer[i]["date"] = dateTime["date"];
		buffer[i]["time"] = dateTime["time"];
		linkRegexp := regexp.MustCompile(`http`)
		if !linkRegexp.MatchString(buffer[i]["link"]) {
			buffer[i]["link"] = fmt.Sprintf("https://tengrinews.kz%s", buffer[i]["link"]);
		}
		buffer[i]["title"] = html.UnescapeString(buffer[i]["title"]);
		buffer[i]["views"] = views;
		buffer[i]["characters"] = fmt.Sprintf("%d", utf8.RuneCountInString(buffer[i]["title"]));
	}

	for _, v := range buffer {
		result = append(result, v);
	}

	return;
}

func (tengry Tengry) query(url string, page int) (string, error) {
	if page != 0 {
		url = fmt.Sprintf(url, fmt.Sprintf("page/%d/", page), tengry.DateBegin, tengry.DateEnd);
		return tools.Request(url, false);
	} else {
		url = fmt.Sprintf(url, "", tengry.DateBegin, tengry.DateEnd);
		return tools.Request(url, false);
	}
}

func (tengry Tengry) getDateTime(val string) (result map[string]string) {
	for k, v := range tools.MonthsDateTime {
		monthRegexp := regexp.MustCompile(k);
		match := monthRegexp.MatchString(val);
		if match {
			replaceFormatDate := monthRegexp.ReplaceAllString(val, fmt.Sprintf("$3-%s-$1 $4", v));
			dateTimeRegexp := regexp.MustCompile("(?P<date>[0-9]+.*?[0-9]+.*?[0-9]+.*?(?P<time>[0-9]+:[0-9]+))");

			searchValue := dateTimeRegexp.FindStringSubmatch(replaceFormatDate);
			result = tools.InstallOneMatchSubexpNames(dateTimeRegexp, searchValue);
			break;
		}
	}

	return
}

func (tengry Tengry) getViews(id int, link string) (string, error) {
	prefix := "tengrinews_ru";
	linkRegexp := regexp.MustCompile(`travel`)
	if linkRegexp.MatchString(link) {
		prefix = "tengritravel_ru";
	}

	url := fmt.Sprintf("https://counter.tengrinews.kz/inc/%s/news/%d", prefix, id);
	request, err := tools.Request(url, false)
	if err != nil {
		return "", err;
	}

	var buffer map[string]int
	err = json.Unmarshal([]byte(request), &buffer)
	if err != nil {
		fmt.Println("error:", err)
		return "", err;
	}

	return strconv.Itoa(buffer["result"]), nil;
}