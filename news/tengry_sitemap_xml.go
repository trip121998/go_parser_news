package news

import (
  "fmt"
  "time"
  "regexp"
  "parserNews/tools"
)

func GetLinksFromSitemap(dateB, dateE string) []string {
	sitemapLinks := getSitemapLinks()
	newsLinks := getAllNewsLink(sitemapLinks, dateB, dateE)
	return newsLinks
}

func getAllNewsLink(links [][]string, dateB, dateE string) []string {
	var newsLinks []string = []string{}
	dateBegin, _ := time.Parse("2006-01-02", dateB)
	dateEnd, _ := time.Parse("2006-01-02", dateE)
	differenceDates := dateEnd.Sub(dateBegin).Hours() / 24;
	
	for i := 0; i <= int(differenceDates); i++ {
		thenDate := dateBegin.AddDate(0, 0, +i).Format("2006-01-02")
		fmt.Println(thenDate)
		var newsLinksIteration []string = []string{}

		for _, link := range links {
			sitemap := getSitemap(link[1])
			regexpSitemapLinks := regexp.MustCompile(fmt.Sprintf(`(?ms)<url>.*?loc>(.*?)<\/loc>.*?%s.*?<\/url>`, thenDate))
			linksSitemap := regexpSitemapLinks.FindAllStringSubmatch(sitemap, -1)
			if len(linksSitemap) != 0 {
				for _, linkSitemap := range linksSitemap {
					newsLinksIteration = append(newsLinksIteration, linkSitemap[1])
				}
			} else {
				if len(newsLinksIteration) != 0 {
					break
				}
			}
		}

		newsLinks = append(newsLinks, newsLinksIteration...)
	}

	return newsLinks
}

var sitemapsBuffer map[string]string

func getSitemap(link string) string {
	_, ok := sitemapsBuffer[link]
	if ok {
		return sitemapsBuffer[link]
	}

	sitemap, _ := tools.Request(link, false)
	return sitemap
}

func getSitemapLinks() [][]string {
	sitemapsXML, _ := tools.Request("https://tengrinews.kz/sitemap-index.xml", false)
	regexpSitemapLinks := regexp.MustCompile("loc>(.*?sitemap-news.*?)<")
	return regexpSitemapLinks.FindAllStringSubmatch(sitemapsXML, -1)
}
