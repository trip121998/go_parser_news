package news

import (
	"fmt"
	"time"
	"regexp"
	"unicode/utf8"
	"parserNews/tools"
	"golang.org/x/net/html"
	// "strconv"
	"encoding/json"
)

type JsonText struct {
	Result string `json:"result"`
	List string `json:"list"`
}

type Sports struct {
	DateBegin string
	DateEnd string
	Chan chan tools.NewsChan
}

func (sports Sports) GetChan() chan tools.NewsChan {
	return sports.Chan;
}

func (sports *Sports) SetChan(newChan chan tools.NewsChan) {
	sports.Chan = newChan
}

func NewSports(dateBegin, dateEnd string) *Sports {
	return &Sports {
		DateBegin: dateBegin,
		DateEnd: dateEnd,
		Chan: make(chan tools.NewsChan),
	}
}

func (sports Sports) SearchAllNews(key int) {
	newsChan := tools.NewsChan{Key: key};
	news := make([]map[string]string, 0, 0);
	fmt.Println("Begin Sports----------", time.Now());

	dateBegin, _ := time.Parse("2006-01-02", sports.DateBegin)
	dateEnd, _ := time.Parse("2006-01-02", sports.DateEnd)
	differenceDates := dateEnd.Sub(dateBegin).Hours() / 24;
	if differenceDates < 0 {
		sports.Chan <- newsChan;
		return;
	}

	for i := 0; i <= int(differenceDates); i++ {
		thenDate := dateEnd.AddDate(0, 0, -i).Format("2006-01-02")
	    count := 0;

	    for {
	    	text, _ := tools.RequestSports(thenDate, count);
		    jsonText := JsonText{};
		    json.Unmarshal([]byte(text), &jsonText)

		    linksRegexp := regexp.MustCompile(`(?ms)<li.*?href="(?P<link>.*?)"|<\/li>$`)
		    searchValue := linksRegexp.FindAllStringSubmatch(jsonText.List, -1);
		    links := tools.InstallAllMatchSubexpNames(linksRegexp, searchValue);

		    for _, v := range links {
		    	url := fmt.Sprintf("https://www.sports.kz%s", v["link"]);
		    	text, err := sports.query(url);
				if err != nil {
					newsChan.Err = err;
					sports.Chan <- newsChan;
					return;
				}
		    	buffer := sports.findNews(text, url)
				if err != nil {
					newsChan.Err = err;
					sports.Chan <- newsChan;
					return;
				}

				if buffer == nil {
					continue;
				}

				news = append(news, buffer);
		    }

		    if jsonText.List == "" {
		    	break;
		    }
		    count += 8;
	    }
	}
	news = tools.ReverseSlice(news);
	fmt.Println("End Sports----------", time.Now());
	newsChan.Value = news;
	sports.Chan <- newsChan;
}

func (sports Sports) findNews(text string, url string) (result map[string]string) {
	regexpStrings := []string{
		`(?ms)news_main_ under_.*?datetype.*?>.*?>(?P<date>.*?)<.*?i>\((?P<time>.*?)\).*?h1>(?P<title>.*?)<`,
		`(?ms)flex text_style_read.*?h1>(?P<title>.*?)<.*?datastyle">((?P<date>.*?\d{4}).*?(?P<time>\d{2}:\d{2})).*?<`,
	}

	var regexpValues *regexp.Regexp;

	for _, v := range regexpStrings {
		regexpBuffer := regexp.MustCompile(v);
		if regexpBuffer.MatchString(text) {
			regexpValues = regexpBuffer;
		}
	}
	if regexpValues == nil {
		return;
	}

	searchValue := regexpValues.FindStringSubmatch(text);
	if len(searchValue) == 0 {
		return;
	}
	values := tools.InstallOneMatchSubexpNames(regexpValues, searchValue);

	values["title"] = html.UnescapeString(values["title"]);
	values["link"] = url;
	values["characters"] = fmt.Sprintf("%d", utf8.RuneCountInString(values["title"]));

	result = values;
	return;
}

func (sports Sports) query(url string) (string, error) {
	return tools.Request(url, true);
}