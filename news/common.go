package news

import (
	"fmt"
	"regexp"
	"parserNews/tools"
	"parserNews/excel"
	"parserNews/analytics"
	"github.com/xuri/excelize/v2"
)



func findNews(text string, regexpString string) (buffer []map[string]string) {
	linksRegexp := regexp.MustCompile(regexpString)
	searchValue := linksRegexp.FindAllStringSubmatch(text, -1);
	buffer = tools.InstallAllMatchSubexpNames(linksRegexp, searchValue);
	return
}

var newsMap map[string]func(dateBegin, dateEnd string) tools.NewsInterface = map[string]func(dateBegin, dateEnd string) tools.NewsInterface{
	"Zakon": func(dateBegin, dateEnd string) tools.NewsInterface { return NewZakon(dateBegin, dateEnd)},
	"Tengry": func(dateBegin, dateEnd string) tools.NewsInterface { return NewTengry4(dateBegin, dateEnd)},
	"Nur": func(dateBegin, dateEnd string) tools.NewsInterface { return NewNur(dateBegin, dateEnd)},
	"Sports": func(dateBegin, dateEnd string) tools.NewsInterface { return NewSports(dateBegin, dateEnd)},
	"SportArena": func(dateBegin, dateEnd string) tools.NewsInterface { return NewSportArena(dateBegin, dateEnd)},
	"Vesti": func(dateBegin, dateEnd string) tools.NewsInterface { return NewVesti(dateBegin, dateEnd)},
}

func GetExcelAllNews(dateBegin, dateEnd string, tengryVersion int, ) (*excelize.File) {
	fmt.Println("GetExcelAllNews")
	countChan := getCountChan()
	chanNewCommon := make(chan tools.NewsChan, countChan)

	for k, v := range tools.StatesNews {
		if v.CheckBox.CheckState() != 2 {
			continue;
		}

		tools.StatesNews[k].NewsParser = newsMap[v.Name](dateBegin, dateEnd);

		tools.StatesNews[k].NewsParser.SetChan(chanNewCommon)
		go tools.StatesNews[k].NewsParser.SearchAllNews(k);
	}

	for i:= 0; i < countChan; i++ {
		bufferNews := tools.NewsChan{}

		select {
		case bufferNews = <-chanNewCommon:
			stateNews := &tools.StatesNews[bufferNews.Key]
			stateNews.News = bufferNews

			if stateNews.News.Err != nil {
				stateNews.CheckImageCheckbox(tools.ImagesCheckboxKeys["failed"])
			} else {
				stateNews.CheckImageCheckbox(tools.ImagesCheckboxKeys["success"])
			}
		}
	}
	addComparison()
	f := excel.Generate();
 
 	return f;
}

func addComparison() {
	var mainStateNews tools.StateNews
	sliceStatesNewsComparison := make([]tools.StateNews, 0, 3)

	for _, stateNews := range tools.StatesNews {
		if stateNews.CheckBox.CheckState() != 2 {
			continue;
		}

		if stateNews.Name == "Zakon" {
			mainStateNews = stateNews
		}

		if tools.SliceContains(tools.ComparisonNews, stateNews.Name) {
			sliceStatesNewsComparison = append(sliceStatesNewsComparison, stateNews)
		}
	}

	if len(sliceStatesNewsComparison) == 2 {
		analytics.Comparison(mainStateNews, sliceStatesNewsComparison)
		analytics.SearchWhyFirstFromTime()
	}
}

func getCountChan() int {
	countChan := 0;

	for _, stateNews := range tools.StatesNews {
		if stateNews.CheckBox.CheckState() != 2 {
			continue
		}

		countChan++
	}

	return countChan
}