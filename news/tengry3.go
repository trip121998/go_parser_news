package news

import (
	"fmt"
	"log"
	"time"
	"sort"
	"regexp"
	"strconv"
	"unicode/utf8"
	"encoding/json"
	"parserNews/tools"
	"golang.org/x/net/html"
	"parserNews/parserErrors"
)

type Tengry3 struct {
	DateBegin string
	DateEnd string
	Chan chan tools.NewsChan
}

func NewTengry3(dateBegin, dateEnd string) *Tengry3 {
	return &Tengry3 {
		DateBegin: dateBegin,
		DateEnd: dateEnd,
		Chan: make(chan tools.NewsChan),
	}
}

var regexpContentMainItemString = `(?ms)(content_main_item".*?href="(?P<link>.*?)".*?item_meta.*?span>(?P<date>.*?)<)|\/body`
var regexpGridItemString = `(?ms)(grid-item.*?title.*?href="(?P<link>.*?(?P<id>\d+).*?)".*?>(?P<title>.*?)<.*?meta.*?span.*?>(?P<datetime>.*?)<)|\/body`;
var regexpBlogPostString = `(?ms)(blog-post alaska.*?date">(?P<datetime>.*?)<.*?href="(?P<link>.*?(?P<id>\d+).*?)">(?P<title>.*?)<)|\/body`

var sitesTengry = map[string]map[string]string{
	"https://tengrinews.kz/news/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/world_news/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/accidents/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/crime/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/events/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/tech/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/gadgets/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/internet/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/culture/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/cinema/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/music/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengriauto.kz/autonews/%s": map[string]string {
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/newseducation/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengritravel.kz/my-country/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengritravel.kz/around-the-world/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengritravel.kz/travel-notes/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengritravel.kz/sayakhat-time/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrisport.kz/tnsport/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrisport.kz/around_sports/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrisport.kz/trends/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrisport.kz/champions-league/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrisport.kz/europa-league/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrisport.kz/ice-hockey-world-cup-2023/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/horoscopes/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/handsomely/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/profitably/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/healthy/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
	"https://tengrinews.kz/curious/%s": map[string]string{
		"regexpNewslineString": regexpContentMainItemString,
		"regexpViewNewsString": "",
	},
}

func (tengry Tengry3) GetChan() chan tools.NewsChan {
	return tengry.Chan;
}

func (tengry *Tengry3) SetChan(newChan chan tools.NewsChan) {
	tengry.Chan = newChan
}

func (tengry Tengry3) SearchAllNews(key int) {
	fmt.Println("Tengry----------", time.Now());
	newsChans := make(chan tools.NewsChan, len(sitesTengry));
	var newsChan = tools.NewsChan{}
	for url, regexpStrings := range sitesTengry {
		go tengry.SearchUrlNews(url, newsChans, regexpStrings);

	}
	news := make([]map[string]string, 0, 0);
	for i := 0; i < len(sitesTengry); i++ {	
		newsChan = <- newsChans;
		if len(newsChan.Value) != 0 {
			fmt.Println(len(newsChan.Value), newsChan.Value)
		}
		if newsChan.Err != nil {
			fmt.Println(newsChan.Err)
			newsChan.Key = key;
			tengry.Chan <- newsChan
		}
		news = append(news, newsChan.Value...);
	}

	news = tools.RemoveDuplicates(news)
	sort.Sort(tools.ByDate(news))
	// TODO
	// for _, value := range news {
	// 	fmt.Println(value["description"], "\n\n")
	// }
	fmt.Println("Tengry End----------", time.Now());
	newsChan.Key = key;
	newsChan.Value = news;
	tengry.Chan <- newsChan;
}

func (tengry Tengry3) SearchUrlNews(
	url string, 
	newsChans chan tools.NewsChan, 
	regexpStrings map[string]string,
) {
	// fmt.Println("Tengry----------", url, time.Now());
	newsChan := tools.NewsChan{};

	news := make([]map[string]string, 0, 0);

	text, err := tengry.query(url, 1);

	if err != nil {
		newsChan.Err = err;
		newsChans <- newsChan;
		return;
	}

	endPage, err := tengry.getPages(text);
	if err != nil {
		newsChan.Err = err;
		newsChans <- newsChan;
		return;
	}

    i := 1;
    for {
    	text, err := tengry.query(url, i);
		if err != nil {
			newsChan.Err = err;
			newsChans <- newsChan;
			return;
		}

		var buffer []map[string]string
		var stop bool

		// _, ok := regexpStrings["regexpViewNewsString"]

		// if ok {
    		buffer, stop, err = tengry.findNewsViaPage(url, text, regexpStrings, i)
    	// } else {
    	// 	buffer, stop, err = tengry.findNews(text, regexpStrings, url, i)
    	// }

		if err != nil {
			newsChan.Err = err;
			newsChans <- newsChan;
			return;
		}
		if buffer != nil {
			news = append(news, buffer...);
		}
		// fmt.Println(endPage == i, endPage, i, url)
		if stop || endPage == i {
			// fmt.Println("tututu", endPage, i, url)
    		break;
    	}

		i++;
    }

	// fmt.Println("Tengry End----------", url, time.Now());
	newsChan.Value = news;
	newsChans <- newsChan;
}

func (tengry Tengry3) getPages(text string) (endPage int,  err error) {
	var bufferEndPage string

	pageRegexpString := `page-link".*?>(?P<page>\d+?)<`;
	pageRegexp := regexp.MustCompile(pageRegexpString)
	searchValue := pageRegexp.FindAllStringSubmatch(text, -1);

	if len(searchValue) != 0 {
	    result := tools.InstallOneMatchSubexpNames(pageRegexp, searchValue[len(searchValue)-1])

	    value, ok := result["page"];
	    if !ok {
	    	err = parserErrors.ERROR_TENGRY_NOT_PAGE;
	    	return;
	    } else {
	    	bufferEndPage = value;
	    }
	} else {
		bufferEndPage = "1";
	}

    endPage, err = strconv.Atoi(bufferEndPage)
    if err != nil {
        return;
    }

    if endPage == 0 {
    	err = parserErrors.ERROR_TENGRY_NOT_PAGE;
    	return;
    }

    return;
}


func (tengry Tengry3) findNews(text string, regexpStrings map[string]string, url string, i int) (result []map[string]string, stop bool, err error) {
	text = getSection(text)
	buffer := findNews(text, regexpStrings["regexpNewslineString"]);

	if len(buffer) == 0 {
		tools.ParseValueToFile(text)
		log.Fatal(regexpStrings, url, i)
	}
	for i := 0; i < len(buffer); i++ {
		dateTime := tengry.getDateTime(buffer[i]["datetime"]);
		if len(dateTime) == 0 {
			fmt.Println(dateTime, buffer[i], url, i, "tut");
			err = fmt.Errorf("TENGRY: datetime not correct");
			return;
		}
		date:= tengry.getDate(dateTime["date"])
		// fmt.Println("asdd--------", tengry.DateBegin, tengry.DateEnd, date, url)
		if tengry.DateBegin > date {
			stop = true;
			break;
		}

		if !(tengry.DateEnd >= date) {
			continue;
		}

		id, err := strconv.Atoi(buffer[i]["id"]);
		if err != nil {
			return result, stop, err;
		}

		views, err := tengry.getViews(id, buffer[i]["link"]);
		if err != nil {
			return result, stop, err;
		}

		buffer[i]["date"] = dateTime["date"];
		buffer[i]["time"] = dateTime["time"];
		buffer[i]["datetime"] = dateTime["datetime"]
		linkRegexp := regexp.MustCompile(`http`)
		if !linkRegexp.MatchString(buffer[i]["link"]) {
			buffer[i]["link"] = fmt.Sprintf("https://tengrinews.kz%s", buffer[i]["link"]);
		}
		buffer[i]["title"] = html.UnescapeString(buffer[i]["title"]);
		buffer[i]["views"] = views;
		buffer[i]["characters"] = fmt.Sprintf("%d", utf8.RuneCountInString(buffer[i]["title"]));

		result = append(result, buffer[i]);
	}

	return;
}

func (tengry Tengry3) findNewsViaPage(url string, text string, regexpStrings map[string]string, i int) (result []map[string]string, stop bool, err error) {
	text = getSection(text)
	regexpLinksAndDates := regexp.MustCompile(regexpStrings["regexpNewslineString"]);
	searchValue := regexpLinksAndDates.FindAllStringSubmatch(text, -1);
	linksAndDates := tools.InstallAllMatchSubexpNames(regexpLinksAndDates, searchValue);

	stop = false;

	if len(linksAndDates) == 0 {
		fmt.Println(url)
	}
	for _, linkAndDate := range linksAndDates {
		regexpHTTP := regexp.MustCompile(`http`);
		if !regexpHTTP.MatchString(linkAndDate["link"]) {
			linkAndDate["link"] = "https://tengrinews.kz"+linkAndDate["link"]
		}
		date:= tengry.getDate(linkAndDate["date"])
		// fmt.Println("asdd--------", tengry.DateBegin, tengry.DateEnd, date, url)
		if tengry.DateBegin > date {
			stop = true;
			break;
		}

		if !(tengry.DateEnd >= date) {
			continue;
		}

		values, err := tengry.getContent(linkAndDate["link"]);
		if err != nil {
			continue;
		}

		dateTime := tengry.getDateTime(values["datetime"]);
		if len(dateTime) == 0 {
			fmt.Println(dateTime, values)
			err = parserErrors.ERROR_TENGRY_DATETIME_NOT_CORRECT;
			return result, stop, err;
		}

		id, err := strconv.Atoi(values["id"]);
		if err != nil {
			return result, stop, err;
		}

		views, err := tengry.getViews(id, values["link"]);
		if err != nil {
			return result, stop, err;
		}

		values["date"] = dateTime["date"];
		values["time"] = dateTime["time"];
		values["datetime"] = dateTime["datetime"]
		values["views"] = views;
		values["title"] = html.UnescapeString(values["title"]);
		values["characters"] = fmt.Sprintf("%d", utf8.RuneCountInString(values["title"]));

		result = append(result, values);
	}
	
	return;
}

func (tengry Tengry3) getDate(date string) string {
	date = tools.Trim(date);
	for k, v := range tools.MonthsTime {
		monthRegexp := regexp.MustCompile(k);
		match := monthRegexp.MatchString(date);
		if match {
			numberRegexp := regexp.MustCompile(`(?ms)\d`)
			match = numberRegexp.MatchString(date)
			if match {
				date = monthRegexp.ReplaceAllString(date, fmt.Sprintf("$3-%s-$1", v));
			} else {
				date = monthRegexp.ReplaceAllString(date, v)
			}
			break;
		}
	}

	return date
}

func (tengry Tengry3) getContent(link string) (result map[string]string, err error) {
	request, err := tools.Request(link, false)
	if err != nil {
		return result, err;
	}

	regexpStrings := []string{
		`(?ms)date-time.*?(?P<datetime>(\d+|Вч|Се).*?)<.*?<h1.*?>(?P<title>.*?)<.*?data-id="(?P<id>.*?)".*?desc"><p>(?P<description>.*?\.)`,
		`(?ms)entry-title">(?P<title>.*?)<\/h1>.*?entry-date.*?">(?P<datetime>.*?)<.*?data-id="(?P<id>.*?)".*?desc"><p>(?P<description>.*?\.)`,
		`(?ms)post-title">(?P<title>.*?)<.*?date">(?P<datetime>.*?)<.*?data-id="(?P<id>.*?)".*?desc"><p>(?P<description>.*?\.)`,
	}

	var regexpValues *regexp.Regexp;

	for _, v := range regexpStrings {
		regexpBuffer := regexp.MustCompile(v);
		if regexpBuffer.MatchString(request) {
			regexpValues = regexpBuffer;
		}
	}
	if regexpValues == nil {
		err = parserErrors.ERROR_TENGRY_NOT_FIND_REGEXP_CONTENT
		return;
	}

	searchValue := regexpValues.FindStringSubmatch(request);
	if len(searchValue) == 0 {
		err = parserErrors.ERROR_TENGRY_NOT_FIND_CONTENT_REGEXP;
		return;
	}
	result = tools.InstallOneMatchSubexpNames(regexpValues, searchValue);
	result["description"] = tengry.sanitaizerDescription(result["description"])
	result["link"] = link;


	dateTime := tengry.getDateTime(result["datetime"]);
	if len(dateTime) == 0 {
		fmt.Println(link, regexpValues, result)
	}

	return;
}

func (tengry Tengry3) sanitaizerDescription(description string) string {
	re := regexp.MustCompile(`(?ms)<.*?>.*?<\/.*?>`)
	return re.ReplaceAllString(description, "")
}

func (tengry Tengry3) query(url string, page int) (string, error) {
	if page != 1 {
		url = fmt.Sprintf(url, fmt.Sprintf("page/%d/", page));
		return tools.Request(url, false);
	} else {
		url = fmt.Sprintf(url, "");
		return tools.Request(url, false);
	}
}

func (tengry Tengry3) getDateTime(val string) (result map[string]string) {
	for k, v := range tools.MonthsDateTime {
		monthRegexp := regexp.MustCompile(k);
		match := monthRegexp.MatchString(val);
		if match {
			replaceFormatDate := monthRegexp.ReplaceAllString(val, fmt.Sprintf("$3-%s-$1 $4", v));
			dateTimeRegexp := regexp.MustCompile("((?P<date>[0-9]+.*?[0-9]+.*?[0-9]+).*?(?P<time>[0-9]+:[0-9]+))");

			searchValue := dateTimeRegexp.FindStringSubmatch(replaceFormatDate);
			result = tools.InstallOneMatchSubexpNames(dateTimeRegexp, searchValue);
			result["datetime"] = result["date"] + " " + result["time"]
			break;
		}
	}

	return
}

func (tengry Tengry3) getViews(id int, link string) (string, error) {
	prefix := "tengrinews_ru";
	linkRegexp := regexp.MustCompile(`travel`)
	if linkRegexp.MatchString(link) {
		prefix = "tengritravel_ru";
	}

	url := fmt.Sprintf("https://counter.tengrinews.kz/inc/%s/news/%d", prefix, id);
	request, err := tools.Request(url, false)
	if err != nil {
		return "", err;
	}

	var buffer map[string]int
	err = json.Unmarshal([]byte(request), &buffer)
	if err != nil {
		fmt.Println("error:", err)
		return "", err;
	}

	return strconv.Itoa(buffer["result"]), nil;
}

func getSection(text string) string {
	var re = regexp.MustCompile(`(?ms)section class="first">.*?<\/section>`)
	if re.MatchString(text) {
		return re.FindString(text)
	}

	return text
}