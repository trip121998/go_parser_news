package news

import (
	"fmt"
	"time"
	"regexp"
	"unicode/utf8"
	"parserNews/tools"
	"golang.org/x/net/html"
	"strconv"
)

type SportArena struct {
	DateBegin string
	DateEnd string
	Chan chan tools.NewsChan
}

func NewSportArena(dateBegin, dateEnd string) *SportArena {
	return &SportArena {
		DateBegin: dateBegin,
		DateEnd: dateEnd,
		Chan: make(chan tools.NewsChan),
	}
}

func (sportArena SportArena) GetChan() chan tools.NewsChan {
	return sportArena.Chan;
}

func (sportArena *SportArena) SetChan(newChan chan tools.NewsChan) {
	sportArena.Chan = newChan
}

func (sportArena SportArena) SearchAllNews(key int) {
	newsChan := tools.NewsChan{Key: key};
	fmt.Println("Begin SportArena----------", time.Now());
	url := "https://sportarena.kz/search/?p=%d";


	text, err := sportArena.query(url, 0);
	if err != nil {
		newsChan.Err = err;
		sportArena.Chan <- newsChan;
		return;
	}

	var bufferEndPage string
	pageRegexpString := `(?ms)\/search\/\?p=(?P<page>\d+)?"`;
	pageRegexp := regexp.MustCompile(pageRegexpString)
	searchValue := pageRegexp.FindAllStringSubmatch(text, -1);
	if len(searchValue) != 0 {
	    result := tools.InstallOneMatchSubexpNames(pageRegexp, searchValue[len(searchValue)-1])

	    value, ok := result["page"];
	    if !ok {
	    	newsChan.Err = fmt.Errorf("no page");
	    	sportArena.Chan <- newsChan;
	    	return;
	    } else {
	    	bufferEndPage = value;
	    }
	} else {
		bufferEndPage = "1";
	}

    endPage, err := strconv.Atoi(bufferEndPage)
    if err != nil {
        newsChan.Err = err;
        sportArena.Chan <- newsChan;
        return;
    }

    if endPage == 0 {
    	newsChan.Err = fmt.Errorf("no pages");
    	sportArena.Chan <- newsChan;
    	return;
    }
    
	news := make([]map[string]string, 0, 0);
	buffer, stop := sportArena.findNews(text);
	news = append(news, buffer...);

	var i = 2;

	if !stop {
	    for {
	    	text, err := sportArena.query(url, i);
	    	if err != nil {
	    		newsChan.Err = err;
	    		sportArena.Chan <- newsChan;
	    		return;
	    	}

	    	buffer, stop := sportArena.findNews(text)
			news = append(news, buffer...);

			if stop {
				break;
			}

	    	i++;
	    }
	}
    news = tools.ReverseSlice(news);

	fmt.Println("End SportArena----------", time.Now(), len(news));
	newsChan.Value = news;
	sportArena.Chan <- newsChan;
}

func (sportArena SportArena) findNews(text string) (result []map[string]string, stop bool) {
	regexpString := `(?ms)s-card__item.*?href="(?P<link>.*?)".*?s-card__title">(?P<title>.*?)<.*?datetime="((?P<date>.*?) (?P<time>.*?))"`;
	buffer := findNews(text, regexpString);

	for i := 0; i < len(buffer); i++ {
		if !(sportArena.DateEnd >= buffer[i]["date"]) {
			continue;
		}

		if sportArena.DateBegin > buffer[i]["date"] {
			stop = true;
			break;
		}
		buffer[i]["title"] = html.UnescapeString(buffer[i]["title"]);
		buffer[i]["link"] = fmt.Sprintf("https://sportarena.kz%s", buffer[i]["link"]);
		buffer[i]["characters"] = fmt.Sprintf("%d", utf8.RuneCountInString(buffer[i]["title"]));

		result = append(result, buffer[i]);
	}

	return;
}

func (sportArena SportArena) query(url string, page int) (string, error) {
	if page != 0 {
		url = fmt.Sprintf(url, page);
	}
	return tools.Request(url, true);
}