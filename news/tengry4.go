package news

import (
	"fmt"
	"time"
	"regexp"
	"strconv"
	"unicode/utf8"
	"encoding/json"
	"parserNews/tools"
	"golang.org/x/net/html"
	"parserNews/parserErrors"
)

type Tengry4 struct {
	DateBegin string
	DateEnd string
	Chan chan tools.NewsChan
}

func NewTengry4(dateBegin, dateEnd string) *Tengry4 {
	return &Tengry4 {
		DateBegin: dateBegin,
		DateEnd: dateEnd,
		Chan: make(chan tools.NewsChan),
	}
}

func (tengry Tengry4) GetChan() chan tools.NewsChan {
	return tengry.Chan;
}

func (tengry *Tengry4) SetChan(newChan chan tools.NewsChan) {
	tengry.Chan = newChan
}

func (tengry Tengry4) SearchAllNews(key int) {
	newsChan := tools.NewsChan{Key: key};
	fmt.Println("Tengry----------", time.Now());
    
	news, err := tengry.findNews()
	if err != nil {
		newsChan.Err = err;
		tengry.Chan <- newsChan;
		return;
	}

	news = tools.BubbleSort(news, "time")

	fmt.Println("Tengry----------", time.Now());
	newsChan.Value = news;
	tengry.Chan <- newsChan;
}

func (tengry Tengry4) getPages(text string) (endPage, countPage int, err error) {
	var bufferEndPage string

	pageRegexpString := `(?ms)href="/search/page/[0-9]+/\?field=all.*?>(?P<page>[0-9]+)<\/a><\/li>`;
	pageRegexp := regexp.MustCompile(pageRegexpString)
	searchValue := pageRegexp.FindAllStringSubmatch(text, -1);

	if len(searchValue) != 0 {
	    result := tools.InstallOneMatchSubexpNames(pageRegexp, searchValue[len(searchValue)-1])

	    value, ok := result["page"];
	    if !ok {
	    	err = parserErrors.ERROR_TENGRY_NOT_PAGE;
	    	return;
	    } else {
	    	bufferEndPage = value;
	    }
	} else {
		bufferEndPage = "1";
	}

    endPage, err = strconv.Atoi(bufferEndPage)
    if err != nil {
        return;
    }

    if endPage == 0 {
    	err = parserErrors.ERROR_TENGRY_NOT_PAGE;
    	return;
    }

    countPage = endPage * 20;
    return;
}

func (tengry Tengry4) findNews() (result []map[string]string, err error) {
	links := GetLinksFromSitemap(tengry.DateBegin, tengry.DateEnd);

	for _, v := range links {
		regexpHTTP := regexp.MustCompile(`http`);
		if !regexpHTTP.MatchString(v) {
			v = "https://tengrinews.kz"+v
		}

		values, err := tengry.getContent(v);
		if err != nil {
			fmt.Println(err)
			continue;
		}

		dateTime := tengry.getDateTime(values["datetime"]);
		if len(dateTime) == 0 {
			fmt.Println(v)
			continue
		}

		if tengry.DateBegin > dateTime["date"] || !(tengry.DateEnd >= dateTime["date"]) {
			fmt.Println(tengry.DateBegin, dateTime["date"], tengry.DateEnd, tengry.DateBegin > dateTime["date"], tengry.DateEnd >= dateTime["date"])
			continue
		}

		id, err := strconv.Atoi(values["id"]);
		if err != nil {
			fmt.Println(err)
			return result, err;
		}

		views, err := tengry.getViews(id, values["link"]);
		if err != nil {
			fmt.Println(err)
			return result, err;
		}

		values["date"] = dateTime["date"];
		values["time"] = dateTime["time"];
		values["datetime"] = dateTime["datetime"]
		values["views"] = views;
		values["title"] = html.UnescapeString(values["title"]);
		values["characters"] = fmt.Sprintf("%d", utf8.RuneCountInString(values["title"]));

		result = append(result, values);
	}

	return;
}

func (tengry Tengry4) getContent(link string) (result map[string]string, err error) {
	request, err := tools.Request(link, false)
	if err != nil {
		return result, err;
	}

	regexpStrings := []string{
		`(?ms)date-time.*?>(?P<datetime>.*?)<.*?<h1.*?>(?P<title>.*?)<.*?data-id="(?P<id>.*?)"`,
		`(?ms)entry-title">(?P<title>.*?)<\/h1>.*?entry-date.*?">(?P<datetime>.*?)<.*?data-id="(?P<id>.*?)"`,
		`(?ms)post-title">(?P<title>.*?)<.*?date">(?P<datetime>.*?)<.*?data-id="(?P<id>.*?)"`,
		`(?ms)date-time.*?<\/a>(?P<datetime>.*?)<.*?head-single">(?P<title>.*?)<.*?data-id="(?P<id>.*?)"`,
	}

	var regexpValues *regexp.Regexp;

	for _, v := range regexpStrings {
		regexpBuffer := regexp.MustCompile(v);
		if regexpBuffer.MatchString(request) {
			regexpValues = regexpBuffer;
		}
	}
	if regexpValues == nil {
		err = parserErrors.ERROR_TENGRY_NOT_FIND_REGEXP_CONTENT
		return;
	}

	searchValue := regexpValues.FindStringSubmatch(request);
	if len(searchValue) == 0 {
		err = parserErrors.ERROR_TENGRY_NOT_FIND_CONTENT_REGEXP;
		return;
	}
	result = tools.InstallOneMatchSubexpNames(regexpValues, searchValue);
	result["link"] = link;

	return;
}

func (tengry Tengry4) query(url string, page int) (string, error) {
	if page != 0 {
		url = fmt.Sprintf(url, fmt.Sprintf("page/%d/", page), tengry.DateBegin, tengry.DateEnd);
		return tools.Request(url, false);
	} else {
		url = fmt.Sprintf(url, "", tengry.DateBegin, tengry.DateEnd);
		return tools.Request(url, false);
	}
}

func (tengry Tengry4) getDateTime(val string) (result map[string]string) {
	for k, v := range tools.MonthsDateTime {
		monthRegexp := regexp.MustCompile(k);
		match := monthRegexp.MatchString(val);
		if match {
			replaceFormatDate := monthRegexp.ReplaceAllString(val, fmt.Sprintf("$3-%s-$1 $4", v));
			dateTimeRegexp := regexp.MustCompile("((?P<date>[0-9]+.*?[0-9]+.*?[0-9]+).*?(?P<time>[0-9]+:[0-9]+))");

			searchValue := dateTimeRegexp.FindStringSubmatch(replaceFormatDate);
			result = tools.InstallOneMatchSubexpNames(dateTimeRegexp, searchValue);
			result["datetime"] = result["date"] + " " + result["time"]
			break;
		}
	}

	if len(result) == 0 {
		fmt.Println(result, val)
	}

	return
}

func (tengry Tengry4) getViews(id int, link string) (string, error) {
	prefix := "tengrinews_ru";
	linkRegexp := regexp.MustCompile(`travel`)
	if linkRegexp.MatchString(link) {
		prefix = "tengritravel_ru";
	}

	url := fmt.Sprintf("https://counter.tengrinews.kz/inc/%s/news/%d", prefix, id);
	request, err := tools.Request(url, false)
	if err != nil {
		return "", err;
	}

	var buffer map[string]int
	err = json.Unmarshal([]byte(request), &buffer)
	if err != nil {
		fmt.Println("error:", err)
		return "", err;
	}

	return strconv.Itoa(buffer["result"]), nil;
}