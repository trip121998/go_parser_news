package excel

import (
	"fmt"
	"parserNews/tools"
	"github.com/xuri/excelize/v2"
)

func Generate() (result *excelize.File){
	f := excelize.NewFile()
	defer func() {
		if err := f.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	styleYellow, err := f.NewStyle(&excelize.Style{
	    Fill: excelize.Fill{Type: "pattern", Color: []string{"ffdf00"}, Pattern: 1},
	})
	styleBlue, err := f.NewStyle(&excelize.Style{
	    Fill: excelize.Fill{Type: "pattern", Color: []string{"60adff"}, Pattern: 1},
	})

	if err != nil {
	    fmt.Println("excel", err)
	}

	f.SetColWidth("Sheet1", "A", "W", 15)
	alphabetPointer := 0;

	for _, stateNews := range tools.StatesNews {
		if stateNews.CheckBox.CheckState() != 2 {
			continue;
		} else if len(stateNews.News.Value) == 0 {
			continue;
		}

		for _, v := range stateNews.Columns {
			f.SetCellValue("Sheet1", alphabet[alphabetPointer]+"1", v);
			alphabetPointer++;
		}
		alphabetPointer += 2;
	}

	alphabetPointer = 0;

	for _, stateNews := range tools.StatesNews {
		if stateNews.CheckBox.CheckState() != 2 {
			continue;
		} else if len(stateNews.News.Value) == 0 {
			continue;
		}

		bufferAlphabetPointer := alphabetPointer;
		countNews := 1;

		for _, values := range stateNews.News.Value {
			alphabetPointer = bufferAlphabetPointer;

			_, okSimilarity := values["similarity"]

			if okSimilarity {
				_, okDouptFul := values["doubtful"]
				if okDouptFul {
					err = f.SetCellStyle("Sheet1", fmt.Sprintf("%s%d", alphabet[alphabetPointer], countNews+1), fmt.Sprintf("%s%d", alphabet[alphabetPointer], countNews+1), styleBlue)
					if err != nil {
						fmt.Println("color install: ", err)
					}
				} else {
					color, okColor := values["color"]

					if okColor {
						style, err := f.NewStyle(&excelize.Style{
						    Fill: excelize.Fill{Type: "pattern", Color: []string{color}, Pattern: 1},
						})
						if err != nil {
							fmt.Println("color install: ", err)
						}
						err = f.SetCellStyle("Sheet1", fmt.Sprintf("%s%d", alphabet[alphabetPointer], countNews+1), fmt.Sprintf("%s%d", alphabet[alphabetPointer+1], countNews+1), style)
						if err != nil {
							fmt.Println("color install: ", err)
						}
					} else {
						err = f.SetCellStyle("Sheet1", fmt.Sprintf("%s%d", alphabet[alphabetPointer], countNews+1), fmt.Sprintf("%s%d", alphabet[alphabetPointer+1], countNews+1), styleYellow)
						if err != nil {
							fmt.Println("color install: ", err)
						}
					}
				}
			}

			for _, column := range columnsSlice {
				value, ok := values[column];
				if !ok && column != "№" {
					continue;
				} else if column == "№" {
					f.SetCellValue("Sheet1", fmt.Sprintf("%s%d", alphabet[alphabetPointer], countNews+1), countNews);
				} else {
					f.SetCellValue("Sheet1", fmt.Sprintf("%s%d", alphabet[alphabetPointer], countNews+1), value);
				}

				alphabetPointer++;
			}
			countNews++;
		}
		alphabetPointer += 2;
	}

	return f;
}