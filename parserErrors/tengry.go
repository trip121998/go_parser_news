package parserErrors

import (
	"fmt"
)

var ERROR_TENGRY_NOT_FIND_REGEXP_CONTENT error = fmt.Errorf("TENGRY: Not find regexp content");
var ERROR_TENGRY_NOT_FIND_CONTENT_REGEXP error = fmt.Errorf("TENGRY: Not find content from regexp");
var ERROR_TENGRY_DATETIME_NOT_CORRECT error = fmt.Errorf("TENGRY: datetime not correct");
var ERROR_TENGRY_NOT_PAGE error = fmt.Errorf("TENGRY: not page");