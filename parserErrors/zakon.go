package parserErrors;

import (
	"fmt"
)

var ERROR_ZAKON_DATE_FORMAT_NOT_CORRECT error = fmt.Errorf("ZAKON: Date format not correct");
var ERROR_ZAKON_NOT_PAGE error = fmt.Errorf("ZAKON: Not page");