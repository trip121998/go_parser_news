package tools

var Html = "<!DOCTYPE html>\n"+
"<html lang=\"en\">\n"+
"<head>\n"+
	"<meta charset=\"UTF-8\">\n"+
	"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"+
	"<title>Parser</title>\n"+
	"<style>\n"+
		"body {\n"+
			"background-color: #000;\n"+
		"}\n"+
		"div {\n"+
			"width: 300px;\n"+
			"height: 280px;\n"+
			"margin:auto;\n"+
			"margin-top: 200px;\n"+
			"box-shadow: 0 0 3px #000;\n"+
			"text-align: center;\n"+
			"padding-top: 20px;\n"+
			"border-radius: 4px;\n"+
			"background-color: #ccc;\n"+
		"}\n"+
		"button {\n"+
			"padding: 10px 20px;\n"+
			"border-radius: 5px;\n"+
		"}\n"+
		"button:hover {\n"+
			"box-shadow: 0;\n"+
			"cursor:pointer;\n"+
		"}\n"+
		"button:active {\n"+
			"box-shadow: 0 0 5px #000 inside;\n"+
		"}\n"+
		"p {\n"+
			"padding: 0px;\n"+
			"margin: 10px;\n"+
		"}\n"+
	"</style>\n"+
"</head>\n"+
"<body>\n"+
	"<div>\n"+
		"<h3>Парсер</h3>\n"+
		"<p>с</p>\n"+
		"<p><input type=\"date\" class=\"dateBegin\"></p>\n"+
		"<p>по</p>\n"+
		"<p><input type=\"date\" class=\"dateEnd\"></p>\n"+
		"<p><button>Сгенирировать</button></p>\n"+
	"</div>\n"+
	"<script type=\"text/javascript\">\n"+
	"let button = document.querySelector(\"button\")\n"+
	"button.onclick = function() {\n"+
			"let dateBegin = document.querySelector('.dateBegin').value;\n"+
			"let dateEnd = document.querySelector('.dateEnd').value;\n"+
			"button.disabled = 'disabled';\n"+
			"if (dateBegin > dateEnd) {\n"+
				"alert('Неправильно указаны даты');\n"+
				"return;\n"+
			"}\n"+
			"var oReq = new XMLHttpRequest();\n"+
			"oReq.open(\"GET\", \"http://localhost:8000/download?date_start=\"+dateBegin+\"&date_end=\"+dateEnd, true);\n"+
			"oReq.responseType = \"arraybuffer\";\n"+
			"let date = new Date();\n"+
			"let month = date.getMonth() + 1 < 10 ? \"0\"+(date.getMonth() + 1) : date.getMonth() + 1\n"+
			"let fileName = `Отчет${date.getFullYear()}${month}${date.getDate()}${date.getHours()}${date.getMinutes()}${date.getSeconds()}.xlsx`;\n"+
			"oReq.onload = function(oEvent) {\n"+
			    "var blob = new Blob([oReq.response], {type: \"application/zip\"});\n"+
			    "var link = document.createElement('a');\n"+
			        "link.setAttribute(\"type\", \"hidden\");\n"+
			        "link.href = window.URL.createObjectURL(blob);\n"+
			        "link.download = fileName;\n"+
			        "document.body.appendChild(link);\n"+
			        "link.click();\n"+
			        "document.body.removeChild(link);\n"+
					"button.removeAttribute('disabled');\n"+
			"};\n"+
			"oReq.send();\n"+
		"}\n"+
	"</script>\n"+
"</body>\n"+
"</html>";