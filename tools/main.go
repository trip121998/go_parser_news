package tools

import (
	"fmt"
	"os"
	"time"
	"net/http"
	"regexp"
	"strings"
	"io/ioutil"
)

func ParseValueToFile(text string) {
    file, err := os.Create("hello.txt")
     
    if err != nil{
        fmt.Println("Unable to create file:", err) 
        os.Exit(1) 
    }
    defer file.Close() 
    file.WriteString(text)
     
    fmt.Println("Done parse value to file.")
}

func ReverseSlice(array []map[string]string) []map[string]string {
	if (len(array) <= 1) {
		return array
	}
	for i := 0; i < len(array)/2; i++ {
		j := len(array) - i - 1
		array[i], array[j] = array[j], array[i]
	}
	return array
}

func InstallOneMatchSubexpNames(re *regexp.Regexp, searchValue []string) map[string]string {
    result := make(map[string]string)
 
    for i, name := range re.SubexpNames() {
        if i != 0 && name != "" {
            result[name] = searchValue[i]
        }
    }

    return result;
}

func InstallAllMatchSubexpNames(re *regexp.Regexp, searchValue [][]string) []map[string]string {
	if (len(searchValue) == 0) {
		return []map[string]string{};
	}
    result := make([]map[string]string, len(searchValue)-1, len(searchValue)-1)

 	for j := 0; j < len(searchValue)-1; j++ {
 		buffer := make(map[string]string);
	    for i, name := range re.SubexpNames() {
	        if i != 0 && name != "" {
	            buffer[name] = searchValue[j][i]
	        }
	    }
	    result[j] = buffer
	}

	return result;
}

func Request(url string, ajax bool) (string, error) {
	method := "GET"

	client := &http.Client {
	}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		return "", err
	}
	req.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:121.0) Gecko/20100101 Firefox/121.0")
	if (ajax) {
		req.Header.Add("Accept", "application/json")
		req.Header.Add("X-Requested-With", "XMLHttpRequest")
	} else {
		req.Header.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8")		
	}


	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	return string(body), nil;
}

func RequestSports(date string, count int) (string, error) {

  url := "https://www.sports.kz/ajax/loadCalendarNews"
  method := "POST"

  payload := strings.NewReader(fmt.Sprintf("date=%s&count=%d", date, count))

  client := &http.Client {
  }
  req, err := http.NewRequest(method, url, payload)

  if err != nil {
    return "", err;
  }
  req.Header.Add("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:123.0) Gecko/20100101 Firefox/123.0")
  req.Header.Add("Accept", "application/json, text/javascript, */*; q=0.01")
  // req.Header.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3")
  // req.Header.Add("Accept-Encoding", "gzip, deflate, br")
  req.Header.Add("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
  req.Header.Add("X-Requested-With", "XMLHttpRequest")
  req.Header.Add("Origin", "https://www.sports.kz")
  req.Header.Add("Connection", "keep-alive")
  req.Header.Add("Referer", "https://www.sports.kz/calendar/2024-03-09")
  req.Header.Add("Cookie", "__ddg1_=13vETYBOnuPR4lV8WGda; PHPSESSID=7239c00417018fdac634fdacdfcb646e")
  req.Header.Add("Sec-Fetch-Dest", "empty")
  req.Header.Add("Sec-Fetch-Mode", "cors")
  req.Header.Add("Sec-Fetch-Site", "same-origin")
  req.Header.Add("TE", "trailers")

  res, err := client.Do(req)
  if err != nil {
    return "", err;
  }
  defer res.Body.Close()

  body, err := ioutil.ReadAll(res.Body)
  if err != nil {
    return "", err;
  }
  return string(body), nil;
}

func Trim(value string) string {
	trimRegexp := regexp.MustCompile(`(?ms)^(\n| +)(.*?)`);
	result := trimRegexp.ReplaceAllString(value, "$2");
	trimRegexp = regexp.MustCompile(`(?ms)(.*?) +$`);
	result = trimRegexp.ReplaceAllString(result, "$1");
	return result;
}

func Strtotime(str string) (int64, error) {
	layout := "2006-01-02 15:04:05"
	t, err := time.Parse(layout, str)
	if err != nil {
		return 0, err
	}
	return t.Unix(), nil
}

func getNowDate() string {
	currentTime := time.Now() 
    return currentTime.Format("2006-01-02")
}

func getYesterdayDate() string {
	yesterday := time.Now().AddDate(0, 0, -1)
	return yesterday.Format("2006-01-02");
}

type ByDate []map[string]string

func (date ByDate) Len() int           { return len(date) }
func (date ByDate) Swap(i, j int)      { date[i], date[j] = date[j], date[i] }
func (date ByDate) Less(i, j int) bool { 
	dateFormatI, _ := time.Parse("2006-01-02 15:04", date[i]["datetime"])
	dateFormatJ, _ := time.Parse("2006-01-02 15:04", date[j]["datetime"])
	return dateFormatI.Before(dateFormatJ) 
}


func SliceContains(slice []string, find string) bool {
	for _, value := range slice {
		if value == find {
			return true
		}
	}

	return false
}

func RemoveDuplicates(elements []map[string]string) []map[string]string {
    encountered := map[string]bool{}
    result := []map[string]string{}

    for _, v := range elements {
        if !encountered[v["title"]] {
            encountered[v["title"]] = true
            result = append(result, v)
        } else {
        	fmt.Println(v["link"])
        }
    }
    return result
}

func BubbleSort(array []map[string]string, key string) []map[string]string {
   for i:=0; i< len(array)-1; i++ {
      for j:=0; j < len(array)-i-1; j++ {
         if (array[j][key] > array[j+1][key]) {
            array[j], array[j+1] = array[j+1], array[j]
         }
      }
   }
   return array
}