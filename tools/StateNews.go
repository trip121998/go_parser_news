package tools

import (
	"github.com/therecipe/qt/widgets"
)

type StateNews struct {
	Name string
	CheckBox *widgets.QCheckBox
	News NewsChan
	NewsParser NewsInterface
	Columns []string
	imagesCheckbox [3]*widgets.QLabel
}

func (SN *StateNews) SetImagesCheckbox(images [3]*widgets.QLabel) {
	SN.imagesCheckbox = images
}

func (SN *StateNews) CheckImageCheckbox(imageKey int) {
	for _, imageCheckbox := range SN.imagesCheckbox {
		imageCheckbox.Hide()
	}

	if imageKey != -1 {
		SN.imagesCheckbox[imageKey].Show()
	}
}